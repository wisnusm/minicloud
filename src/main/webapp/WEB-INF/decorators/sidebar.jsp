
<nav class="navbar" style="height: 100%; position: fixed;"
	role="navigation">
	<aside class="main-sidebar">

		<section class="sidebar">
			<c:forEach items="${menuModelList}" var="menuModel">
				<a href="${contextName}/${menuModel.controller}.html">
					${menuModel.namaMenu}
				</a>
				<br/>
			</c:forEach>
		</section>
		<!-- /.sidebar -->
	</aside>
</nav>