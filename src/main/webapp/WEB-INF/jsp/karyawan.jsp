<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman Karyawan</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-karyawan">
			<thead>
				<tr>
					<th>NIK</th>
					<th>Nama Karyawan</th>
					<th>Departemen</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-karyawan">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu Karyawan</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataKaryawan();
	/* Fungsi javascript untuk mengambil data dari tabel karyawan */
	function loadDataKaryawan() {		
		$.ajax({
			url: 'karyawan/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-karyawan').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel karyawan */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'karyawan/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-karyawan-add', function() {
			$.ajax({
				url: 'karyawan/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Karyawan Tersimpan");
					loadDataKaryawan();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-karyawan').on('click','#btn-edit', function() {
			var nikUbah = $(this).val();
			$.ajax({
				url: 'karyawan/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {nik:nikUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-karyawan-edit', function() {
			$.ajax({
				url: 'karyawan/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Karyawan Berhasil Diubah");
					loadDataKaryawan();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-karyawan').on('click','#btn-detail', function() {
			var nikRincian = $(this).val();
			$.ajax({
				url: 'karyawan/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {nik:nikRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-karyawan').on('click','#btn-delete', function() {
			var nikHapus = $(this).val();
			$.ajax({
				url: 'karyawan/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {nik:nikHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-karyawan-delete', function() {
			$.ajax({
				url: 'karyawan/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Karyawan Berhasil Dihapus");
					loadDataKaryawan();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


