<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
	<tr>
		<td>${perusahaanModel.kodePerusahaan}</td>
		<td>${perusahaanModel.namaPerusahaan}</td>
		<td>${perusahaanModel.telpPerusahaan}</td>
		<td>
			<button type="button" id="btn-edit" value="${perusahaanModel.kodePerusahaan}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${perusahaanModel.kodePerusahaan}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${perusahaanModel.kodePerusahaan}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>