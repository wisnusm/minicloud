<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-perusahaan-add">
	<div class="modal-header"><h3>Menu Tambah</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Perusahaan</label>
			<div class="col-md-6">
				<input type="text" id="kodePerusahaan" name="kodePerusahaan" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Perusahaan</label>
			<div class="col-md-6">
				<input type="text" id="namaPerusahaan" name="namaPerusahaan" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Telepon Perusahaan</label>
			<div class="col-md-6">
				<input type="text" id="telpPerusahaan" name="telpPerusahaan" />
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();" class="btn btn-primary">Simpan</button>
		</div>	
	</div>
</form>


<script type="text/javascript">
	function validasi() {
		var kodePerusahaan = document.getElementById('kodePerusahaan');
		var namaPerusahaan = document.getElementById('namaPerusahaan');
	 	var telpPerusahaan = document.getElementById('telpPerusahaan');		
		
		var hasil = true;

		if (kodePerusahaan.value == "") {
			kodePerusahaan.style.borderColor = "red";
			alert('Kode Perusahaan kosong!');
			hasil = false;
		} else {
			kodePerusahaan.style.borderColor = "none";
		}

		if (namaPerusahaan.value == "") {
			namaPerusahaan.style.borderColor = "red";
			alert('Nama Perusahaan kosong!');
			hasil = false;
		} else {
			namaPerusahaan.style.borderColor = "none";
		}

		
		
		if (telpPerusahaan.value == "") {
			telpPerusahaan.style.borderColor = "red";
			alert('Telepon Perusahaan kosong!');
			hasil = false;

		} else {
			// cek lagi apakah isinya angka atau mengandung huruf
			if (isNaN(telpPerusahaan.value)) {
				// isNan maksudnya apakah nilainya Not a Number?				
				telpPerusahaan.style.borderColor = "red";
				alert('Nomor telepon tidak sesuai format!');
				hasil = false;
			} else {
				// jika nilainya angka semua
				telpPerusahaan.style.borderColor = "none";
			}
		}
			
		
		
		return hasil;
	}
</script>