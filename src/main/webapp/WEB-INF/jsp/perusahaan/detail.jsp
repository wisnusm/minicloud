<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-perusahaan-detail">
	<div class="modal-header"><h3>Menu Rincian</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Perusahaan</label>
			<div class="col-md-6">
				<input type="text" id="kodePerusahaan" name="kodePerusahaan" value="${perusahaanModel.kodePerusahaan}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Perusahaan</label>
			<div class="col-md-6">
				<input type="text" id="namaPerusahaan" name="namaPerusahaan" value="${perusahaanModel.namaPerusahaan}" disabled="disabled"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Telepon Perusahaan</label>
			<div class="col-md-6">
				<input type="text" id="telpPerusahaan" name="telpPerusahaan" value="${perusahaanModel.telpPerusahaan}" disabled="disabled"/>
			</div>
		</div>	
	</div>
</form>
