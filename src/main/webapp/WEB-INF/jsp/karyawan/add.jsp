<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-karyawan-add">
	<div class="modal-header"><h3>Menu Tambah</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">NIK</label>
			<div class="col-md-6">
				<input type="text" id="nik" name="nik" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Karyawan</label>
			<div class="col-md-6">
				<input type="text" id="namaKaryawan" name="namaKaryawan" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Departemen Karyawan</label>
			<div class="col-md-6">
				<select id="kodeDepartemen" name="kodeDepartemen" class="form-control">
					<c:forEach items="${departemenModelList}" var="departemenModel"> <!-- ini di looping -->
						<option value="${departemenModel.kodeDepartemen}">
							${departemenModel.namaDepartemen}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Perusahaan Karyawan</label>
			<div class="col-md-6">
				<select id="kodePerusahaan" name="kodePerusahaan" class="form-control">
					<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
						<option value="${perusahaanModel.kodePerusahaan}">
							${perusahaanModel.namaPerusahaan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();" class="btn btn-primary">Simpan</button>
		</div>	
	</div>
</form>


<script type="text/javascript">
	function validasi() {
		var nik = document.getElementById('nik');
		var namaKaryawan = document.getElementById('namaKaryawan');
	 	
		var hasil = true;

		if (nik.value == "") {
			nik.style.borderColor = "red";
			alert('NIK kosong!');
			hasil = false;
		} else {
			nik.style.borderColor = "none";
		}

		if (namaKaryawan.value == "") {
			namaKaryawan.style.borderColor = "red";
			alert('Nama Karyawan kosong!');
			hasil = false;
		} else {
			namaKaryawan.style.borderColor = "none";
		}
		
		
		return hasil;
	}
</script>