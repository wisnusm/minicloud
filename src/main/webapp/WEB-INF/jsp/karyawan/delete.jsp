<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-karyawan-delete">
	<div class="modal-header"><h3>Menu Hapus</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">NIK</label>
			<div class="col-md-6">
				<input type="hidden" id="nik" name="nik" value="${karyawanModel.nik}"/>
				<input type="text" id="nikDisplay" name="nikDisplay" value="${karyawanModel.nik}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Karyawan</label>
			<div class="col-md-6">
				<input type="text" id="namaKaryawan" name="namaKaryawan" value="${karyawanModel.namaKaryawan}" disabled="disabled"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Departemen Karyawan</label>
			<div class="col-md-6">
				<select id="kodeDepartemen" name="kodeDepartemen" class="form-control" disabled="disabled">
					<c:forEach items="${departemenModelList}" var="departemenModel"> <!-- ini di looping -->
						<option value="${departemenModel.kodeDepartemen}"
							<c:if test="${departemenModel.kodeDepartemen==karyawanModel.kodeDepartemen}">
								<c:out value='selected'/>
							</c:if>
						>
							${departemenModel.namaDepartemen}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Perusahaan Karyawan</label>
			<div class="col-md-6">
				<select id="kodePerusahaan" name="kodePerusahaan" class="form-control" disabled="disabled">
					<c:forEach items="${perusahaanModelList}" var="perusahaanModel">
						<option value="${perusahaanModel.kodePerusahaan}"
							<c:if test="${perusahaanModel.kodePerusahaan==karyawanModel.kodePerusahaan}">
								<c:out value='selected'/>
							</c:if>
						>
							${perusahaanModel.namaPerusahaan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>>
		<div class="modal-footer">
			Apakah anda yakin menghapus data ini? 
			<button type="submit" class="btn btn-danger">Yakin</button>
		</div>		
	</div>
</form>
