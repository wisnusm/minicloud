<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${karyawanModelList}" var="karyawanModel">
	<tr>
		<td>${karyawanModel.nik}</td>
		<td>${karyawanModel.namaKaryawan}</td>
		<td>${karyawanModel.departemenModel.namaDepartemen}</td>
		<td>
			<button type="button" id="btn-edit" value="${karyawanModel.nik}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${karyawanModel.nik}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${karyawanModel.nik}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>