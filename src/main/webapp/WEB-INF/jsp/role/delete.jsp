<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-role-delete">
	<div class="modal-header"><h3>Menu Hapus</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Role</label>
			<div class="col-md-6">
				<input type="hidden" id="kodeRole" name="kodeRole" value="${roleModel.kodeRole}"/>
				<input type="text" id="kodeRoleDisplay" name="kodeRoleDisplay" value="${roleModel.kodeRole}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Role</label>
			<div class="col-md-6">
				<input type="text" id="namaRole" name="namaRole" value="${roleModel.namaRole}" disabled="disabled"/>
			</div>
		</div>
		<div class="modal-footer">
			Apakah anda yakin menghapus data ini? 
			<button type="submit" class="btn btn-danger">Yakin</button>
		</div>		
	</div>
</form>
