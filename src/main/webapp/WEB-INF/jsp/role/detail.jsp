<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-role-detail">
	<div class="modal-header"><h3>Menu Rincian</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Role</label>
			<div class="col-md-6">
				<input type="text" id="kodeRole" name="kodeRole" value="${roleModel.kodeRole}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Role</label>
			<div class="col-md-6">
				<input type="text" id="namaRole" name="namaRole" value="${roleModel.namaRole}" disabled="disabled"/>
			</div>
		</div>
	</div>
</form>
