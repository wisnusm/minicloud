<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${roleModelList}" var="roleModel">
	<tr>
		<td>${roleModel.kodeRole}</td>
		<td>${roleModel.namaRole}</td>
		<td>
			<button type="button" id="btn-edit" value="${roleModel.kodeRole}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${roleModel.kodeRole}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${roleModel.kodeRole}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>