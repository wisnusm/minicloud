<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>


<form action="#" method="get" id="form-role-edit">
	<div class="modal-header"><h3>Menu Edit</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Role</label>
			<div class="col-md-6">
				<input type="hidden" id="kodeRole" name="kodeRole" value="${roleModel.kodeRole}"/>
				<input type="text" id="kodeRoleDisplay" name="kodeRoleDisplay" value="${roleModel.kodeRole}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Role</label>
			<div class="col-md-6">
				<input type="text" id="namaRole" name="namaRole" value="${roleModel.namaRole}"/>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();"class="btn btn-primary">Simpan</button>
		</div>		
	</div>
</form>

<script type="text/javascript">
	function validasi() {
		var kodeRole = document.getElementById('kodeRole');
		var namaRole = document.getElementById('namaRole');
	 	
		var hasil = true;

		if (kodeRole.value == "") {
			kodeRole.style.borderColor = "red";
			alert('Kode Role kosong!');
			hasil = false;
		} else {
			kodeRole.style.borderColor = "none";
		}

		if (namaRole.value == "") {
			namaRole.style.borderColor = "red";
			alert('Nama Role kosong!');
			hasil = false;
		} else {
			namaRole.style.borderColor = "none";
		}
		
	
		return hasil;
	}
</script>