<form id="form-item" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="kode_item" name="kode_item" class="form-control" value="${item.kode_item}">
					
		<div class="form-group">
			<label class="control-label col-md-3">Kode item : </label>
			<div class="col-md-6">
				<input type="text" id="kode_item" name="kode_item" 
					class="form-control"  style="background-color: white" value="${item.kode_item}" maxlength="10" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama item : </label>
			<div class="col-md-6">
				<input type="text" id="nama_item" name="nama_item" 
					class="form-control" style="background-color: white" value="${item.nama_item}" readonly="readonly">
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="control-label col-md-3">Jumlah Item : </label>
			<div class="col-md-6">
				<input type="text" id="jumlah_item" name="jumlah_item" 
					class="form-control" style="background-color: white" value="${item.jumlah_item}" readonly="readonly">
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="control-label col-md-3">Kategori : </label>
			<div class="col-md-6">
				<input type="text" id="kode_kategori" name="kode_kategori" 
					class="form-control" style="background-color: white" value="${item.kategoriModel.nama_kategori}" readonly="readonly">
			</div>
		</div>
		
		
		
		<div class="form-group">
			<label class="control-label col-md-3">Supplier : </label>
			<div class="col-md-6">
				<input type="text" id="kode_supplier" name="kode_supplier" 
					class="form-control" style="background-color: white" value="${item.supplierModel.nama_supplier}" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Harga Jual : </label>
			<div class="col-md-6">
				<input type="text" id="harga_jual" name="harga_jual" 
					class="form-control" style="background-color: white" value="${item.harga_jual}" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Keterangan : </label>
			<div class="col-md-6">
				<input type="text" id="ket" name="ket" 
					class="form-control" style="background-color: white" value="${item.ket}" readonly="readonly">
			</div>
		</div>
		
		
	</div>	
	
</form>
