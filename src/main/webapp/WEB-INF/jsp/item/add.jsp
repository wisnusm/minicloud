<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-item-add" method="post">
	<div class="form-horizontal">
	
		<div class="form-group">
			<label class="control-label col-md-3">Kode Item</label>
			<div class="col-md-6">
				<input type="text" id="kodeItem" name="kodeItem"
					 class="form-control" maxlength="7" >
			</div>			
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Nama Item</label>
			<div class="col-md-6">
			
				<input type="text" id="namaItem" name="namaItem"
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Jumlah Item</label>
			<div class="col-md-6">
				<input type="text" onkeypress="return doAngka(this)" id=jumlahItem name="jumlahItem"
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="control-label col-md-3">Kategori</label>
			<div class="col-md-6">
				<select class="form-control" id="kodeKategori" name="kodeKategori">
					<c:forEach items="${kategoriModelList}" var="kategoriModel">
						<option value="${kategoriModel.kodeKategori}"> 
							${kategoriModel.namaKategori}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="control-label col-md-3">Supplier</label>
			<div class="col-md-6">
				<select class="form-control" id="kodeSupplier" name="kodeSupplier">
					<c:forEach items="${supplierModelList}" var="supplierModel">
						<option value="${supplierModel.kodeSupplier}"> 
							${supplierModel.namaSupplier}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		
		
		<div class="form-group">
			<label class="control-label col-md-3">Harga Jual</label>
			<div class="col-md-6">
				<input type="text" onkeypress="return doAngka(this)" id="hargaJualItem" name="hargaJualItem"
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Keterangan</label>
			<div class="col-md-6">
				<input type="text" id="keteranganItem" name="keteranganItem" 
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
	
		<div class="modal-footer">
			<button type="submit" onclick="doValidasiMax();" class="btn btn-success" id="btn_save">Simpan</button>
		</div>
	</div>
</form>
<script>
function doAngka(evt) {
	  var charAngka = (evt.which) ? evt.which : event.keyCode		  
	   if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57)))
	    return false;
	  return true;  
}

//Fungsi Validasi
function doValidasiMax() {
	var kode_item = document.getElementById("kode_item");		
	var nama_item = document.getElementById("nama_item");
	var harga_jual = document.getElementById("harga_jual");
	var ket = document.getElementById("ket");
	
	//Validasi Inputan Kosong
	if (kode_item.value == "" ) {
		kode_item.setCustomValidity("Kode item Tidak Boleh Kosong");		
	}else if (nama_item.value == ""){
		nama_item.setCustomValidity("Nama item Tidak Boleh Kosong");		
	}else if(harga_jual.value == ""){
		harga_jual.setCustomValidity("Harga Jual Tidak Boleh Kosong");		
	}else if(ket.value == ""){
		ket.setCustomValidity("Keterangan Tidak Boleh Kosong");		
	}

}

</script>