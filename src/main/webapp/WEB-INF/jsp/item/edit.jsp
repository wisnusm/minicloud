<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-item-edit" method="post">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-2">Kode Item</label>
			<div class="col-md-6">
				<input type="text" id="kodeItem" name="kodeItem"  class="form-control" value="${itemModel.kodeItem}" readonly="readonly"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Item</label>
			<div class="col-md-6">
				<input type="text" id="namaItem" name="namaItem" 
					oninput="setCustomValidity('')" class="form-control" required="required" value="${itemModel.namaItem}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jumlah Item</label>
			<div class="col-md-6">
				<input type="text" onkeypress="return doAngka(this)" id="jumlahItem" name="jumlahItem"
					oninput="setCustomValidity('')" class="form-control" required="required" value="${itemModel.jumlahItem}"/>
			</div>
		</div>
			
		<div class="form-group">
			<label class="control-label col-md-2">Kategori</label>
			<div class="col-md-6">
				<select id="kode_kategori" name="kode_kategori" class="form-control">
					<c:forEach var="kategori" items="${kategoriList}">
						<option value="${kategori.kode_kategori}"> ${kategori.nama_kategori}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Supplier :</label>
			<div class="col-md-6">
				<select id="kode_supplier" name="kode_supplier" class="form-control">
					<c:forEach var="supplier" items="${supplierList}">
						<option value="${supplier.kode_supplier}"> ${supplier.nama_supplier}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Harga Jual :</label>
			<div class="col-md-6">
				<input type="text" onkeypress="return doAngka(this)"  id="hargaJualItem" name="hargaJualItem"
					oninput="setCustomValidity('')" class="form-control" required="required" value="${itemModel.hargaJual}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan :</label>
			<div class="col-md-6">
				<input type="text" id="keteranganItem" name="keteranganItem"
					oninput="setCustomValidity('')" class="form-control" required="required" value="${itemModel.keteranganItem}">
			</div>
		</div>
		
	
		<div class="modal-footer">
			<button type="submit"  onclick="doValidasiMax();" class="btn btn-success" id="btn_update">Ubah</button>
		</div>
	</div>
</form>
<script>
function doAngka(evt) {
	  var charAngka = (evt.which) ? evt.which : event.keyCode		  
	   if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57)))
	    return false;
	  return true;  
}
//Fungsi Validasi
function doValidasiMax() {
	var kode_item = document.getElementById("kode_item");		
	var nama_item = document.getElementById("nama_item");
	var harga_jual = document.getElementById("harga_jual");
	var ket = document.getElementById("ket");
	var maxlength = 15;
	//Validasi Inputan Kosong
	if (kode_item.value == "" ) {
		kode_item.setCustomValidity("Kode item Tidak Boleh Kosong");		
	}else if (nama_item.value == ""){
		nama_item.setCustomValidity("Nama item Tidak Boleh Kosong");		
	}else if(harga_jual.value == ""){
		harga_jual.setCustomValidity("Harga Jual Tidak Boleh Kosong");		
	}else if(ket.value == ""){
		ket.setCustomValidity("Keterangan Tidak Boleh Kosong");		
	}

}

</script>