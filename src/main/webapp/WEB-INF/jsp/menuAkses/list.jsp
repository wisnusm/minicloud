<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${menuAksesModelList}" var="menuAksesModel">
	<tr>
		<td>${menuAksesModel.kodeMenuAkses}</td>
		<td>${menuAksesModel.roleModel.namaRole}</td>
		<td>${menuAksesModel.menuModel.namaMenu }</td>
		<td>
			<button type="button" id="btn-edit" value="${menuAksesModel.kodeMenuAkses}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${menuAksesModel.kodeMenuAkses}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${menuAksesModel.kodeMenuAkses}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>