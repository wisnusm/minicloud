<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-menuAkses-edit">
	<div class="modal-header"><h3>Menu Ubah</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode MenuAkses</label>
			<div class="col-md-6">
				<input type="hidden" id="kodeMenuAkses" name="kodeMenuAkses" value="${menuAksesModel.kodeMenuAkses}" />
				<input type="text" id="kodeMenuAksesDisplay" name="kodeMenuAksesDisplay" value="${menuAksesModel.kodeMenuAkses}" disabled="disabled"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Role</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control">
					<c:forEach items="${roleModelList}" var="roleModel"> <!-- ini di looping -->
						<option value="${roleModel.kodeRole}"
							<c:if test="${roleModel.kodeRole==menuAksesModel.kodeRole}">
								<c:out value='selected'/>
							</c:if>
						>
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Menu</label>
			<div class="col-md-6">
				<select id="kodeMenu" name="kodeMenu" class="form-control">
					<c:forEach items="${menuModelList}" var="menuModel">
						<option value="${menuModel.kodeMenu}"
							<c:if test="${menuModel.kodeMenu==menuAksesModel.kodeMenu}">
								<c:out value='selected'/>
							</c:if>
						>
							${menuModel.namaMenu}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();" class="btn btn-primary">Simpan</button>
		</div>	
	</div>
</form>


<script type="text/javascript">
	function validasi() {
		var kodeMenuAkses = document.getElementById('kodeMenuAkses');

		var hasil = true;

		if (kodeMenuAkses.value == "") {
			kodeMenuAkses.style.borderColor = "red";
			alert('Kode MenuAkses kosong!');
			hasil = false;
		} else {
			kodeMenuAkses.style.borderColor = "none";
		}		
		return hasil;
	}
</script>