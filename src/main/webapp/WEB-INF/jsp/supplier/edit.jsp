<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-supplier-edit" method="post">
	<div class="form-horizontal">

		<div class="form-group">
			<label class="control-label col-md-3">Kode Supplier</label>
			<div class="col-md-6">
				<input type="text" id="kodeSupplier" name="kodeSupplier"
					oninput="setCustomValidity('')" class="form-control" required="required" value="${supplierModel.kodeSupplier}" maxlength="7" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Supplier</label>
			<div class="col-md-6">
				<input type="text" id="namaSupplier" name="namaSupplier"
					oninput="setCustomValidity('')" class="form-control" required="required" value="${supplierModel.namaSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tipe</label>
			<div class="col-md-6">
				<select class="form-control" id="tipeSupplier" name="tipeSupplier" >
					<option value="PT" 
						<c:if test="${supplierModel.tipeSupplier=='PT'}">
							<c:out value='selected'/>
						</c:if>
					>PT - Perusahaan Terbatas</option>
					<option value="CV"
						<c:if test="${supplierModel.tipeSupplier=='CV'}">
							<c:out value='selected'/>
						</c:if>
					>CV - Perusahaan Menengah</option>
					<option value="Koperasi"
						<c:if test="${supplierModel.tipeSupplier=='Koperasi'}">
							<c:out value='selected'/>
						</c:if>
					>Koperasi</option>
					<option value="UMKM"
						<c:if test="${supplierModel.tipeSupplier=='UMKM'}">
							<c:out value='selected'/>
						</c:if>
					>UMKM</option>
					<option value="Individu"
						<c:if test="${supplierModel.tipeSupplier=='Individu'}">
							<c:out value='selected'/>
						</c:if>
					>Individu</option>	
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">PIC Supplier</label>
			<div class="col-md-6">
				<input type="text" id="picSupplier" name="picSupplier" 
					oninput="setCustomValidity('')" class="form-control" required="required" value="${supplierModel.picSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">No Telepon</label>
			<div class="col-md-6">
				<input type="text" onkeypress="return doAngka(this)" id="teleponSupplier" name="teleponSupplier"
					oninput="setCustomValidity('')" class="form-control" required="required" maxlength="13" value="${supplierModel.teleponSupplier}" >
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">E-mail</label>
			<div class="col-md-6">
				<input type="text" id="emailSupplier" name="emailSupplier" placeholder="Alamat E-mail Supplier"
					oninput="setCustomValidity('')" class="form-control" required="required" value="${supplierModel.emailSupplier}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Alamat</label>
			<div class="col-md-6">
				<textarea class="form-control" rows="5" cols=" 10" id="alamatSupplier" name="alamatSupplier" 
					oninput="setCustomValidity('')" required="required" >${supplierModel.alamatSupplier}</textarea>				
			</div>
		</div>
	
		<div class="modal-footer">
			<button type="submit"  onclick="doValidasiMax();" class="btn btn-success" id="btn_update">Ubah</button>
		</div>
	</div>
</form>
<script>
function doAngka(evt) {
	  var charAngka = (evt.which) ? evt.which : event.keyCode		  
	   if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57)))
	    return false;
	  return true;  
}
//Fungsi Validasi
function doValidasiMax() {
	var kode_sup = document.getElementById("kode_supplier");		
	var nama_sup = document.getElementById("nama_supplier");
	var pic_sup = document.getElementById("pic_supplier");
	var alamat = document.getElementById("alamat");
	var email = document.getElementById("email");
	var tlp = document.getElementById("tlp");
	var maxlength = 15;
	
	//Validasi Inputan Kosong
	if (kode_sup.value == "" ) {
		kode_sup.setCustomValidity("Kode Supplier Tidak Boleh Kosong");		
	}else if (pic_sup.value == ""){
		pic_sup.setCustomValidity("PIC Supplier Tidak Boleh Kosong");		
	}else if(nama_sup.value == ""){
		nama_sup.setCustomValidity("Nama Supplier Tidak Boleh Kosong");		
	}else if(alamat.value == ""){
		alamat.setCustomValidity("Alamat Supplier Tidak Boleh Kosong");		
	}else if(email.value == ""){
		email.setCustomValidity("Alamat Email Tidak Boleh Kosong");		
	}else if(tlp.value == ""){
		tlp.setCustomValidity("Nomor Telepon Tidak Boleh Kosong");		
	}
}

</script>