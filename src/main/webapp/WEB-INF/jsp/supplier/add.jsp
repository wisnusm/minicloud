<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-supplier-add" method="post">
	<div class="form-horizontal">

		<div class="form-group">
			<label class="control-label col-md-3">Kode Supplier</label>
			<div class="col-md-6">
				<input type="text" id="kodeSupplier" name="kodeSupplier"
					 class="form-control" value="${kodeSupplierGenerator}" >
			</div>			
		</div>

		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Supplier</label>
			<div class="col-md-6">
				<input type="text" id="namaSupplier" name="namaSupplier"
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tipe</label>
			<div class="col-md-6">
				<select class="form-control" id="tipeSupplier" name="tipeSupplier" >	
					<option value="PT">PT - Perusahaan Terbatas</option>
					<option value="CV">CV - Perusahaan Menengah</option>
					<option value="Koperasi">Koperasi</option>
					<option value="UMKM">UMKM</option>
					<option value="Individu">Individu</option>	
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">PIC Supplier</label>
			<div class="col-md-6">
				<input type="text" id="picSupplier" name="picSupplier"
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">No Telepon</label>
			<div class="col-md-6">
				<input type="text" id="teleponSupplier" name="teleponSupplier"
					oninput="setCustomValidity('')" required class="form-control" required="required" maxlength="13">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Email</label>
			<div class="col-md-6">
				<input type="text" id="emailSupplier" name="emailSupplier" placeholder="mart@mart138.com"
					oninput="setCustomValidity('')" class="form-control" required="required" >
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Alamat</label>
			<div class="col-md-6">
				<textarea class="form-control"  rows="5" cols=" 10" id="alamatSupplier" name="alamatSupplier" oninput="setCustomValidity('')"></textarea>				
			</div>
		</div>
		
		
 		

		<div class="modal-footer">
			<button type="submit" onclick="doValidasiMax();" class="btn btn-success" id="btn_save" >Simpan</button>
			<input type="hidden" onclick="doValidasiMax();" value="select" />
		</div>
	</div>
</form>

<script>
//fungsi untuk validasi no telp
function doAngka(evt) {
	  var charAngka = (evt.which) ? evt.which : event.keyCode		  
	   if ((charAngka > 31) && ((charAngka < 48) || (charAngka > 57)))
	    return false;
	  return true;  
}


//Fungsi Validasi
function doValidasiMax() {
	var kode_sup = document.getElementById("kode_supplier");		
	var nama_sup = document.getElementById("nama_supplier");	
	var pic = document.getElementById("pic_supplier");
	var tlp = document.getElementById("tlp");
	var email = document.getElementById("email");
	var alamat = document.getElementById("alamat");
	
	
	//Validasi Inputan Kosong
	if (kode_sup.value == "" ) {
		kode_sup.setCustomValidity("Kode Supplier Tidak Boleh Kosong");		
	}else if(nama_supplier.value == ""){
		nama_sup.setCustomValidity("Nama Supplier Tidak Boleh Kosong");		
	}else if(pic.value == ""){
		pic.setCustomValidity("PIC Supplier Tidak Boleh Kosong");		
	}else if (tlp.value == "") {
		tlp.setCustomValidity("No Telepon Tidak Boleh Kosong")
	}else if(email.value == ""){
		email.setCustomValidity("Email Tidak Boleh Kosong");		
	} else if (alamat.value == "") {
		alamat.setCustomValidity("Alamat Tidak Boleh Kosong");
	}
}

</script>