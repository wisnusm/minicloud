<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${supplierModelList}" var="supplierModel">
	<tr>
		<td>${supplierModel.kodeSupplier}</td>
		<td>${supplierModel.namaSupplier}</td>
		<td>${supplierModel.tipeSupplier}</td>	
		<td>${supplierModel.xCreatedByUserSupplier.karyawanModel.perusahaanModel.namaPerusahaan}</td>	
		<td>
			<button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${supplierModel.kodeSupplier }"><i class="fa fa-edit"></i></button>
			<button type="button" id="btn-delete" class="btn btn-danger btn-xs btn-delete" value="${supplierModel.kodeSupplier }"><i class="fa fa-trash"></i></button>
			<button type="button" id="btn-detail" class="btn btn-info btn-xs btn-detail" value="${supplierModel.kodeSupplier}"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>