<form id="form-supplier" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="kode_supplier" name="kode_supplier" class="form-control" value="${supplier.kode_supplier}">
					
		<div class="form-group">
			<label class="control-label col-md-3">Kode Supplier	: </label>
			<div class="col-md-6">
				<input type="text" id="kode_supplier" name="kode_supplier" 
					class="form-control"  style="background-color: white" value="${supplier.kode_supplier}" maxlength="10" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Supplier	: </label>
			<div class="col-md-6">
				<input type="text" id="nama_supplier" name="nama_supplier" 
					class="form-control" style="background-color: white" value="${supplier.nama_supplier}" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tipe : </label>
			<div class="col-md-6">
				<input type="text" id="tipe" name="tipe" 
					class="form-control" style="background-color: white" value="${supplier.tipe}" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">PIC Supplier : </label>
			<div class="col-md-6">
				<input type="text" id="pic_supplier" name="pic_supplier" 
					class="form-control" style="background-color: white" value="${supplier.pic_supplier}" readonly="readonly">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">No Telepon : </label>
			<div class="col-md-6">
				<input type="text" id="tlp" name="tlp"
					class="form-control" maxlength="12" style="background-color: white" value="${supplier.tlp}" readonly="readonly">
			</div>
		</div>
	
	
	<div class="form-group">
			<label class="control-label col-md-3">E-mail : </label>
			<div class="col-md-6">
				<input type="text" id="email" name="email" 
					class="form-control" style="background-color: white" value="${supplier.email}" readonly="readonly">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Alamat : </label>
			<div class="col-md-6">
				<textarea  class="form-control"id="alamat" name="alamat" 
					style="background-color: white" readonly="readonly">${supplier.alamat}				
				</textarea>				
			</div>
		</div>
		
	</div>
		
		
	
</form>
