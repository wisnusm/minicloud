<form id="form-supplier-delete" method="get">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="kodeSupplier" name="kodeSupplier" class="form-control" value="${supplierModel.kodeSupplier}">
		
		<div class="form-group">
			<label class="control-label col-md-3">Kode Supplier</label>
			<div class="col-md-6">
				<input type="text" id="kodeSupplierDisplay" name="kodeSupplierDisplay" 
					class="form-control" style="background-color: white" value="${supplierModel.kodeSupplier}" maxlength="10">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Nama Supplier</label>
			<div class="col-md-6">
				<input type="text" id="namaSupplier" name="namaSupplier" 
					class="form-control" style="background-color: white" value="${supplierModel.namaSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tipe</label>
			<div class="col-md-6">
				<input type="text" id="tipe" name="tipe" 
					class="form-control" style="background-color: white" value="${supplierModel.tipeSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">PIC Supplier</label>
			<div class="col-md-6">
				<input type="text" id="pic_supplier" name="pic_supplier" 
					class="form-control" style="background-color: white" value="${supplierModel.picSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">No Telepon</label>
			<div class="col-md-6">
				<input type="text" id="tlp" name="tlp"
					class="form-control" maxlength="12" style="background-color: white" value="${supplierModel.teleponSupplier}">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">E-mail</label>
			<div class="col-md-6">
				<input type="text" id="email" name="email" 
					class="form-control" style="background-color: white" value="${supplierModel.emailSupplier}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Alamat</label>
			
			<div class="col-md-6">
				<textarea class="form-control" id="alamat" name="alamat" 
					style="background-color: white" readonly="readonly">${supplierModel.alamatSupplier}				
				</textarea>				
			</div>
		</div>
		
		
		
		
		
		
		<div class="form-group">
			<label class="col-md-10">Apakah anda yakin ingin menghapus data Supplier ${supplier.nama_supplier} ?</label>				
		</div>
		
		
	</div>
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Hapus</button>
	</div>
	
</form>
