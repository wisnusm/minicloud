<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${userModelList}" var="userModel">
	<tr>
		<td>${userModel.kodeUser}</td>
		<td>${userModel.username}</td>
		<td>
			<button type="button" id="btn-edit" value="${userModel.kodeUser}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${userModel.kodeUser}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${userModel.kodeUser}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>