<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-user-detail">
	<div class="modal-header"><h3>Menu Rincian</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode User</label>
			<div class="col-md-6">
				<input type="text" id="kodeUserDisplay" name="kodeUserDisplay" value="${userModel.kodeUser}" disabled="disabled"/>
				<input type="hidden" id="kodeUser" name="kodeUser" value="${userModel.kodeUser}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Username</label>
			<div class="col-md-6">
				<input type="text" id="username" name="username" value="${userModel.username}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Password</label>
			<div class="col-md-6">
				<input type="password" id="password" name="password" value="${userModel.password}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Role</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control" disabled="disabled">
					<c:forEach items="${roleModelList}" var="roleModel"> <!-- ini di looping -->
						<option value="${roleModel.kodeRole}"
							<c:if test="${roleModel.kodeRole==produkModel.kodeRole}">
								<c:out value='selected'/>
							</c:if>
						>
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Karyawan</label>
			<div class="col-md-6">
				<select id="nik" name="nik" class="form-control" disabled="disabled">
					<c:forEach items="${karyawanModelList}" var="karyawanModel">
						<option value="${karyawanModel.nik}"
							<c:if test="${karyawanModel.nik==produkModel.nik}">
								<c:out value='selected'/>
							</c:if>
						>
							${karyawanModel.namaKaryawan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</div>
</form>
