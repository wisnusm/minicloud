<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-user-edit">
	<div class="modal-header"><h3>Menu Ubah</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode User</label>
			<div class="col-md-6">
				<input type="text" id="kodeUserDisplay" name="kodeUserDisplay" value="${userModel.kodeUser}" disabled="disabled"/>
				<input type="hidden" id="kodeUser" name="kodeUser" value="${userModel.kodeUser}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Username</label>
			<div class="col-md-6">
				<input type="text" id="username" name="username" value="${userModel.username}"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Password</label>
			<div class="col-md-6">
				<input type="password" id="password" name="password" value="${userModel.password}" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Role</label>
			<div class="col-md-6">
				<select id="kodeRole" name="kodeRole" class="form-control">
					<c:forEach items="${roleModelList}" var="roleModel"> <!-- ini di looping -->
						<option value="${roleModel.kodeRole}"
							<c:if test="${roleModel.kodeRole==produkModel.kodeRole}">
								<c:out value='selected'/>
							</c:if>
						>
							${roleModel.namaRole}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Karyawan</label>
			<div class="col-md-6">
				<select id="nik" name="nik" class="form-control">
					<c:forEach items="${karyawanModelList}" var="karyawanModel">
						<option value="${karyawanModel.nik}"
							<c:if test="${karyawanModel.nik==produkModel.nik}">
								<c:out value='selected'/>
							</c:if>
						>
							${karyawanModel.namaKaryawan}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();" class="btn btn-primary">Simpan</button>
		</div>	
	</div>
</form>


<script type="text/javascript">
	function validasi() {
		var kodeUser = document.getElementById('kodeUser');
		var username = document.getElementById('username');
		var password = document.getElementById('password');
		var hasil = true;

		if (kodeUser.value == "") {
			kodeUser.style.borderColor = "red";
			alert('Kode User kosong!');
			hasil = false;
		} else {
			kodeUser.style.borderColor = "none";
		}

		if (username.value == "") {
			username.style.borderColor = "red";
			alert('Username kosong!');
			hasil = false;
		} else {
			username.style.borderColor = "none";
		}
		
		if (password.value == "") {
			password.style.borderColor = "red";
			alert('Password kosong!');
			hasil = false;
		} else {
			password.style.borderColor = "none";
		}

		
		
		
		return hasil;
	}
</script>