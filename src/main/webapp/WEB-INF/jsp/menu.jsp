<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman Menu</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-menu">
			<thead>
				<tr>
					<th>Kode Menu</th>
					<th>Nama Menu</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-menu">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu Menu</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataMenu();
	/* Fungsi javascript untuk mengambil data dari tabel menu */
	function loadDataMenu() {		
		$.ajax({
			url: 'menu/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-menu').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel menu */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'menu/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-menu-add', function() {
			$.ajax({
				url: 'menu/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Menu Tersimpan");
					loadDataMenu();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-menu').on('click','#btn-edit', function() {
			var kodeMenuUbah = $(this).val();
			$.ajax({
				url: 'menu/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {kodeMenu:kodeMenuUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-menu-edit', function() {
			$.ajax({
				url: 'menu/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Menu Berhasil Diubah");
					loadDataMenu();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-menu').on('click','#btn-detail', function() {
			var kodeMenuRincian = $(this).val();
			$.ajax({
				url: 'menu/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {kodeMenu:kodeMenuRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-menu').on('click','#btn-delete', function() {
			var kodeMenuHapus = $(this).val();
			$.ajax({
				url: 'menu/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {kodeMenu:kodeMenuHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-menu-delete', function() {
			$.ajax({
				url: 'menu/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Menu Berhasil Dihapus");
					loadDataMenu();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


