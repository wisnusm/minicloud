<% request.setAttribute("contextName", request.getContextPath()); %>

<div class="panel" style="background: white; margin-top: 40px; min-height: 620px">

	<div class="box-header">
		<br/>
		<h1 class="box-title">Halaman Kategori</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
		<!-- <button type="button" id="btn-edit" class="btn btn-primary pull-right">Edit</button>	
		<button type="button" id="btn-delete" class="btn btn-primary pull-right">Delete</button>
		<button type="button" id="btn-detail" class="btn btn-primary pull-right">Detail</button>
	 -->
	</div>
	
	<div class="box-body">
	
		<div>
			<form action="#" method="get" id="form-kategori-search">
				<input type="text" name="keyword" id="keyword" class="form-input"/>
				<select name="tipe" id="tipe" class="form-input">
					<option value="kodeKategori">Kode Kategori</option>
					<option value="namaKategori">Nama Kategori</option>
				</select>
				<button type="submit" class="btn btn-warning">Cari</button>
			</form>			
		</div>
		
		<table class="table" id="tbl-kategori">
			<thead>
				<tr>
					<th>Kode Kategori</th>
					<th>Nama Kategori</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-kategori">
			
			</tbody>
		</table>
	
	</div>

	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content"> 
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>Form Menu Kategori</h4>
				</div>
				<div class="modal-body">
					<!-- div yg class modal-body itu isinya adalah jsp yang akan dishow -->
					
				</div>
			</div>
		</div>
	</div>
	
</div>

<script>

	loadDataKategori(); /* ini utk load fungsinya saat kategori.jsp dipanggil */

	/* fungsi javascript untuk ngeload data dari table Kategori */
	function loadDataKategori() {
		/* ajax untuk load data */
		$.ajax({
			url:'kategori/list.html',
			type:'get',
			dataType:'html',
			success: function(result) {
				$('#list-data-kategori').html(result);
			}
		});
		/* fungsi ajax untuk load data */
	}
	/* Akhir fungsi javascript untuk ngeload data dari table Kategori */

	/* Document ready itu, setelah halaman siap, function dijalankan */
	$(document).ready(function() {
		
		/* fungsi jquery untuk cari data */
		$('#form-kategori-search').on('submit', function() {	
			var keyword = document.getElementById('keyword').value;
			var tipe = document.getElementById('tipe').value;
			
			$.ajax({
				url:'kategori/cari.html',
				type:'get', 
				dataType:'html',  /* tipeCari adalah var yg dilempar ke Controller */
				data:{keywordCari:keyword, tipeCari:tipe}, /*tipe adalah var id dari form di jsp ini*/
				success: function(result){
					$('#list-data-kategori').html(result);
				}
				
				
			});
			return false;
		});
		/* fungsi jquery untuk cari data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit','#form-kategori-delete', function() {	
			$.ajax({
				url:'kategori/simpan_delete.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data Kategori terhapus!");
					loadDataKategori(); /* reload list */
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit','#form-kategori-edit', function() {	
			$.ajax({
				url:'kategori/simpan_edit.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data Kategori terubah!");
					loadDataKategori(); /* reload list */
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save data */
		$('#modal-input').on('submit','#form-kategori-add', function() {	
			$.ajax({
				url:'kategori/simpan_add.json', /* json itu utk kirim data k controller */
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); /* hide utk utk close pop up */
					alert("Data Kategori tersimpan!");
					loadDataKategori(); /* reload list */
				}
			});
			return false; /* false fungsinya utk agar tidak refresh, jd alertnya jalan */
		});
		/* fungsi jquery untuk save data */
		
		/* fungsi jquery pop up detail */
		$('#list-data-kategori').on('click','#btn-detail', function() {	
			var kodeKategoriBtnDetail = $(this).val(); /* artinya get value dari button detail */
			$.ajax({
				url:'kategori/detail.html', 
				type:'get', 
				dataType:'html',
				data:{kodeKategori:kodeKategoriBtnDetail},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery pop up delete */
		$('#list-data-kategori').on('click','#btn-delete', function() {	
			var kodeKategoriBtnDelete = $(this).val(); /* artinya get value dari button delete */
			$.ajax({
				url:'kategori/delete.html', 
				type:'get', 
				dataType:'html',
				data:{kodeKategori:kodeKategoriBtnDelete},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery pop up edit */
		$('#list-data-kategori').on('click','#btn-edit', function() {
			var kodeKategoriBtnEdit = $(this).val(); /* artinya get value dari button edit */
			$.ajax({
				url:'kategori/edit.html', 
				type:'get', 
				dataType:'html',
				data:{kodeKategori:kodeKategoriBtnEdit},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery utk pengganti onclick di javascript */
		$('#btn-tambah').on('click', function() {
			
			/* ajax itu untuk pengganti action, memanggil url request di controller */
			$.ajax({
				url:'kategori/tambah.html', /* url itu action di form */
				type:'get', /* type itu method */
				dataType:'html',/*  dataType itu extensi akhir dari url */
				success: function(result){
					
					/* munculin jsp dalam bentuk pop up */
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');
					/* munculin jsp dalam bentuk pop up */
					
				}
			});
			/* Akhir ajax itu untuk pengganti action, memanggil url request di controller */
			
		});
		/* Akhir fungsi jquery utk pengganti onclick di javascript */
		
	});
	/* Akhir Document ready itu, setelah halaman siap, function dijalankan */
	
</script>