<% request.setAttribute("contextName", request.getContextPath()); %>

<div class="panel" style="background: white; margin-top: 40px; min-height: 620px">

	<div class="box-header">
		<br/>
		<h1 class="box-title">Halaman Supplier</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
		
	</div>
	
	<div class="box-body">
	
		<div>
			<form action="#" method="get" id="form-supplier-search">
				<input type="text" name="keyword" id="keyword" class="form-input"/>
				<select name="tipe" id="tipe" class="form-input">
					<option value="kodeSupplier">Kode Supplier</option>
					<option value="namaSupplier">Nama Supplier</option>
					
				</select>
				<button type="submit" class="btn btn-warning">Cari</button>
			</form>			
		</div>
		
		<table class="table" id="tbl-supplier">
			<thead>
				<tr>
					<th>Kode Supplier</th>
					<th>Nama Supplier</th>
					<th>Tipe Supplier</th>
					<th>Created By Supplier</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-supplier">
			
			</tbody>
		</table>
	
	</div>

	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content"> 
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>Form Menu Supplier</h4>
				</div>
				<div class="modal-body">
					<!-- div yg class modal-body itu isinya adalah jsp yang akan dishow -->
					
				</div>
			</div>
		</div>
	</div>
	
</div>

<script>

	loadDataSupplier(); 

	/* fungsi javascript untuk ngeload data dari table Supplier */
	function loadDataSupplier() {
		/* ajax untuk load data */
		$.ajax({
			url:'supplier/list.html',
			type:'get',
			dataType:'html',
			success: function(result) {
				$('#list-data-supplier').html(result);
			}
		});
		/* fungsi ajax untuk load data */
	}
	/* Akhir fungsi javascript untuk ngeload data dari table Supplier */

	/* Document ready itu, setelah halaman siap, function dijalankan */
	$(document).ready(function() {
		
		/* fungsi jquery untuk cari data */
		$('#form-supplier-search').on('submit', function() {	
			var keyword = document.getElementById('keyword').value;
			var tipe = document.getElementById('tipe').value;
			
			$.ajax({
				url:'Supplier/cari.html',
				type:'get', 
				dataType:'html',  
				data:{keywordCari:keyword, tipeCari:tipe}, 
				success: function(result){
					$('#list-data-supplier').html(result);
				}
			});
			return false;
		});
		/* fungsi jquery untuk cari data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit','#form-supplier-delete', function() {	
			$.ajax({
				url:'supplier/simpan_delete.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data Supplier terhapus!");
					loadDataSupplier(); 
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit','#form-supplier-edit', function() {	
			$.ajax({
				url:'supplier/simpan_edit.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data Supplier terubah!");
					loadDataSupplier(); 
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save data */
		$('#modal-input').on('submit','#form-supplier-add', function() {	
			$.ajax({
				url:'supplier/simpan_add.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					
					if (result.kodeSupplierExist=='yes') {
						alert("Data Supplier dengan kode '"+result.kodeSupplier+"' sudah ada di DB! ");
					}else if (result.namaSupplierExist=="yes") {
						alert("Data Supplier dengan nama '"+result.namaSupplier+"' sudah ada di DB! ");
					} else {
						$("#modal-input").modal('hide'); 
						alert("Data Supplier tersimpan!");
						loadDataSupplier(); 
					}
					
					
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save data */
		
		/* fungsi jquery pop up detail */
		$('#list-data-supplier').on('click','#btn-detail', function() {	
			var kodeSupplierBtnDetail = $(this).val(); 
			$.ajax({
				url:'supplier/detail.html', 
				type:'get', 
				dataType:'html',
				data:{kodeSupplier:kodeSupplierBtnDetail},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery pop up delete */
		$('#list-data-supplier').on('click','#btn-delete', function() {	
			var kodeSupplierBtnDelete = $(this).val(); 
			$.ajax({
				url:'supplier/delete.html', 
				type:'get', 
				dataType:'html',
				data:{kodeSupplier:kodeSupplierBtnDelete},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery pop up edit */
		$('#list-data-supplier').on('click','#btn-edit', function() {
			var kodeSupplierBtnEdit = $(this).val(); 
			$.ajax({
				url:'supplier/edit.html', 
				type:'get', 
				dataType:'html',
				data:{kodeSupplier:kodeSupplierBtnEdit},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery utk pengganti onclick di javascript */
		$('#btn-tambah').on('click', function() {			
			$.ajax({
				url:'supplier/tambah.html', 
				type:'get', 
				dataType:'html',
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');					
				}
			});
		});
		/* Akhir fungsi jquery utk pengganti onclick di javascript */
		
	});
	/* Akhir Document ready itu, setelah halaman siap, function dijalankan */
	
</script>