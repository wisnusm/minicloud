<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman Perusahaan</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-perusahaan">
			<thead>
				<tr>
					<th>Kode Perusahaan</th>
					<th>Nama Perusahaan</th>
					<th>Telepon Perusahaan</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-perusahaan">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu Perusahaan</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataPerusahaan();
	/* Fungsi javascript untuk mengambil data dari tabel perusahaan */
	function loadDataPerusahaan() {		
		$.ajax({
			url: 'perusahaan/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-perusahaan').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel perusahaan */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'perusahaan/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-perusahaan-add', function() {
			$.ajax({
				url: 'perusahaan/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Perusahaan Tersimpan");
					loadDataPerusahaan();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-perusahaan').on('click','#btn-edit', function() {
			var kodePerusahaanUbah = $(this).val();
			$.ajax({
				url: 'perusahaan/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {kodePerusahaan:kodePerusahaanUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-perusahaan-edit', function() {
			$.ajax({
				url: 'perusahaan/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Perusahaan Berhasil Diubah");
					loadDataPerusahaan();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-perusahaan').on('click','#btn-detail', function() {
			var kodePerusahaanRincian = $(this).val();
			$.ajax({
				url: 'perusahaan/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {kodePerusahaan:kodePerusahaanRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-perusahaan').on('click','#btn-delete', function() {
			var kodePerusahaanHapus = $(this).val();
			$.ajax({
				url: 'perusahaan/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {kodePerusahaan:kodePerusahaanHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-perusahaan-delete', function() {
			$.ajax({
				url: 'perusahaan/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Perusahaan Berhasil Dihapus");
					loadDataPerusahaan();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


