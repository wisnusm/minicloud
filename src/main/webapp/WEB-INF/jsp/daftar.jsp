<% request.setAttribute("contextName", request.getContextPath()); %>
<div>
	<br/>
	<h1>Halaman Daftar</h1>
	Selamat datang
	<a href="${contextName}/halaman_hasil_daftar.html"  >Hasil Daftar</a>
	
	<form action="halaman_hasil_daftar.html" method="get" id="form-daftar"  >
		Input String <input type="text" name="inputString" id="inputString"/> <br/>
		Input Integer <input type="text" name="inputInteger" id="inputInteger"/> <br/>
		
		<button type="submit" onclick="return validasi();">Hasil</button>
		
	</form>
</div>

<!-- Part Javascriptnya -->
<script>
	function validasi() {
		var inputString = document.getElementById('inputString');
		var inputInteger = document.getElementById('inputInteger');
		
		var hasil = true;
		
		if (inputString.value=='') {
			inputString.style.borderColor = "red";
			alert('inputString kosong!');
			hasil = false
		} else {
			inputString.style.borderColor = "none";
		}
		
		if (inputInteger.value=='') {
			inputInteger.style.borderColor = "red";
			alert('inputInteger kosong!');
			hasil = false
		} else {
			
			//dicek lagi apakah isinya angka atau ada huruf
			if (isNaN(inputInteger.value)) {
				// isNaN maksudnya apakah nilainya not a number
				inputInteger.style.borderColor = "red";
				alert('inputInteger ada huruf!');
				hasil = false
			} else {
				// else artinya jika nilainya angka
				inputInteger.style.borderColor = "none";
			}
		}
		
		return hasil;
	}
</script>
<!-- Part Javascriptnya -->