<% request.setAttribute("contextName", request.getContextPath()); %>

<div class="box-header">
	<h1 class="box-title">Halaman Sub Detail</h1>
	<button type="button" id="btn-close" class="btn btn-danger pull-right">Close</button>
</div>

<script>
	/* Document ready itu, setelah halaman siap, function dijalankan */
	$(document).ready(function() {
		
 		/* fungsi jquery pop up sub detail */
		$('#btn-close').on('click', function() {	
			$.ajax({
				url:'kategori/detail.html', 
				type:'get', 
				dataType:'html',
				success: function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
	});
	/* Document ready itu, setelah halaman siap, function dijalankan */

</script>