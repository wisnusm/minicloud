
<div class="form-horizontal">	

	<form action="#" method="get" id="form-kategori-add">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Kategori</label>
			<div class="col-md-6">
				<input type="text" name="kodeKategori" class="form-input"/>
			</div>
		</div>
		
		<div class="form-group"> 
			<label class="control-label col-md-3">Nama Kategori</label>
			<div class="col-md-6">
				<input type="text" name="namaKategori" class="form-input"/>
			</div>
		</div>
		
		<div class="form-group">
			
			<label class="control-label col-md-3">Keterangan</label>
			<div class="col-md-6">
				<textarea rows="5" cols="30" name="keteranganKategori" class="form-input"></textarea>
			</div>
		</div>
		
		<div class="modal-footer">
			<button type="submit" class="btn btn-success pull-right">Simpan</button>
		</div>
		
		
	</form>
	
</div>