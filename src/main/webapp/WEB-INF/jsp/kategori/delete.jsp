<h1>Halaman Delete</h1>

<div class="form-horizontal">	

	<form action="#" method="get" id="form-kategori-delete">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Kategori</label>
			<div class="col-md-6">
				<input type="hidden" name="kodeKategori" value="${kategoriModel.kodeKategori}"/>
				<input type="text" name="kodeKategoriDisplay" class="form-input" value="${kategoriModel.kodeKategori}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group"> 
			<label class="control-label col-md-3">Nama Kategori</label>
			<div class="col-md-6">
				<input type="text" name="namaKategori" class="form-input" value="${kategoriModel.namaKategori}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			
			<label class="control-label col-md-3">Keterangan</label>
			<div class="col-md-6">
				<textarea rows="5" cols="30" name="keteranganKategori" class="form-input" disabled="disabled">${kategoriModel.keteranganKategori}</textarea>
			</div>
		</div>
		
		<div class="modal-footer">
			Apakah anda yakin hapus data ini?
			<button type="submit" class="btn btn-success pull-right">Ya, Hapus</button>
		</div>
		
		
	</form>
	
</div>