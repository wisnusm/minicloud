<h1>Halaman Detail</h1>

<div class="form-horizontal">	

	<form action="#" method="get" id="form-kategori">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Kategori</label>
			<div class="col-md-6">
				<input type="text" name="kodeKategori" class="form-input" value="${kategoriModel.kodeKategori}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group"> 
			<label class="control-label col-md-3">Nama Kategori</label>
			<div class="col-md-6">
				<input type="text" name="namaKategori" class="form-input" value="${kategoriModel.namaKategori}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			
			<label class="control-label col-md-3">Keterangan</label>
			<div class="col-md-6">
				<textarea rows="5" cols="30" name="keteranganKategori" class="form-input" disabled="disabled">${kategoriModel.keteranganKategori}</textarea>
			</div>
		</div>
		
		
	</form>
	
</div>