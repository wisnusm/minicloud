<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman User</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-user">
			<thead>
				<tr>
					<th>Kode User</th>
					<th>Username</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-user">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu User</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataUser();
	/* Fungsi javascript untuk mengambil data dari tabel user */
	function loadDataUser() {		
		$.ajax({
			url: 'user/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-user').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel user */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'user/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-user-add', function() {
			$.ajax({
				url: 'user/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data User Tersimpan");
					loadDataUser();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-user').on('click','#btn-edit', function() {
			var kodeUserUbah = $(this).val();
			$.ajax({
				url: 'user/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {kodeUser:kodeUserUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-user-edit', function() {
			$.ajax({
				url: 'user/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data User Berhasil Diubah");
					loadDataUser();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-user').on('click','#btn-detail', function() {
			var kodeUserRincian = $(this).val();
			$.ajax({
				url: 'user/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {kodeUser:kodeUserRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-user').on('click','#btn-delete', function() {
			var kodeUserHapus = $(this).val();
			$.ajax({
				url: 'user/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {kodeUser:kodeUserHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-user-delete', function() {
			$.ajax({
				url: 'user/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data User Berhasil Dihapus");
					loadDataUser();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


