<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman Departemen</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-departemen">
			<thead>
				<tr>
					<th>Kode Departemen</th>
					<th>Nama Departemen</th>
					<th>Telepon Departemen</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-departemen">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu Departemen</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataDepartemen();
	/* Fungsi javascript untuk mengambil data dari tabel departemen */
	function loadDataDepartemen() {		
		$.ajax({
			url: 'departemen/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-departemen').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel departemen */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'departemen/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-departemen-add', function() {
			$.ajax({
				url: 'departemen/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Departemen Tersimpan");
					loadDataDepartemen();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-departemen').on('click','#btn-edit', function() {
			var kodeDepartemenUbah = $(this).val();
			$.ajax({
				url: 'departemen/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {kodeDepartemen:kodeDepartemenUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-departemen-edit', function() {
			$.ajax({
				url: 'departemen/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Departemen Berhasil Diubah");
					loadDataDepartemen();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-departemen').on('click','#btn-detail', function() {
			var kodeDepartemenRincian = $(this).val();
			$.ajax({
				url: 'departemen/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {kodeDepartemen:kodeDepartemenRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-departemen').on('click','#btn-delete', function() {
			var kodeDepartemenHapus = $(this).val();
			$.ajax({
				url: 'departemen/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {kodeDepartemen:kodeDepartemenHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-departemen-delete', function() {
			$.ajax({
				url: 'departemen/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Departemen Berhasil Dihapus");
					loadDataDepartemen();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


