<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman Role</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-role">
			<thead>
				<tr>
					<th>Kode Role</th>
					<th>Nama Role</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-role">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu Role</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataRole();
	/* Fungsi javascript untuk mengambil data dari tabel role */
	function loadDataRole() {		
		$.ajax({
			url: 'role/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-role').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel role */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'role/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-role-add', function() {
			$.ajax({
				url: 'role/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Role Tersimpan");
					loadDataRole();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-role').on('click','#btn-edit', function() {
			var kodeRoleUbah = $(this).val();
			$.ajax({
				url: 'role/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {kodeRole:kodeRoleUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-role-edit', function() {
			$.ajax({
				url: 'role/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Role Berhasil Diubah");
					loadDataRole();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-role').on('click','#btn-detail', function() {
			var kodeRoleRincian = $(this).val();
			$.ajax({
				url: 'role/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {kodeRole:kodeRoleRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-role').on('click','#btn-delete', function() {
			var kodeRoleHapus = $(this).val();
			$.ajax({
				url: 'role/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {kodeRole:kodeRoleHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-role-delete', function() {
			$.ajax({
				url: 'role/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data Role Berhasil Dihapus");
					loadDataRole();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


