<%	request.setAttribute("contextName", request.getContextPath());%>

<div class="panel" style="background: white; margin-top: 40px; min-heigth: 620px">
	<div class="box-header">
		<h1 class="box-title">Halaman MenuAkses</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
	</div>
	
	<div class="box-body">
		<table class="table" id="tbl-menuAkses">
			<thead>
				<tr>
					<th>Kode MenuAkses</th>
					<th>Role</th>
					<th>Menu</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-menuAkses">
			
			</tbody>
		</table>
	</div>
	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h1>Form Menu MenuAkses</h1>
				</div>
				<div class="modal-body">
					<!-- div yang class modal body itu isinya adalah jsp yang akan dishow -->
				</div>
			</div>
		</div>
	</div>
	

</div>

<script>
	loadDataMenuAkses();
	/* Fungsi javascript untuk mengambil data dari tabel menuAkses */
	function loadDataMenuAkses() {		
		$.ajax({
			url: 'menuAkses/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-data-menuAkses').html(result);
			}
		});
	}
	/* Akhir fungsi javascript untuk mengambil data dari tabel menuAkses */
	
	/* Fungsi yang dijalankan setelah halaman siap */
	$(document).ready(function() {
		/* Fungsi jquery untuk button tambah */
		$('#btn-tambah').on('click', function() {
			$.ajax({
				url: 'menuAkses/tambah.html',
				type: 'get',
				dataType: 'html',
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button tambah */
		
		/* Fungsi jquery untuk menyimpan data */
		$('#modal-input').on('submit','#form-menuAkses-add', function() {
			$.ajax({
				url: 'menuAkses/tambah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data MenuAkses Tersimpan");
					loadDataMenuAkses();
				}
			});
		});
		/* Akhir fungsi jquery untuk menyimpan data */
		
		/* Fungsi jquery untuk button ubah */
		$('#list-data-menuAkses').on('click','#btn-edit', function() {
			var kodeMenuAksesUbah = $(this).val();
			$.ajax({
				url: 'menuAkses/ubah.html',
				type: 'get',
				dataType: 'html',
				data: {kodeMenuAkses:kodeMenuAksesUbah},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button ubah */
		
		/* Fungsi jquery untuk menyimpan hasil ubah */
		$('#modal-input').on('submit','#form-menuAkses-edit', function() {
			$.ajax({
				url: 'menuAkses/ubah_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data MenuAkses Berhasil Diubah");
					loadDataMenuAkses();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil ubah */
		
		/* Fungsi jquery untuk button rincian */
		$('#list-data-menuAkses').on('click','#btn-detail', function() {
			var kodeMenuAksesRincian = $(this).val();
			$.ajax({
				url: 'menuAkses/rincian.html',
				type: 'get',
				dataType: 'html',
				data: {kodeMenuAkses:kodeMenuAksesRincian},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button rincian */
		
		/* Fungsi jquery untuk button hapus */
		$('#list-data-menuAkses').on('click','#btn-delete', function() {
			var kodeMenuAksesHapus = $(this).val();
			$.ajax({
				url: 'menuAkses/hapus.html',
				type: 'get',
				dataType: 'html',
				data: {kodeMenuAkses:kodeMenuAksesHapus},
				success: function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		/* Akhir fungsi jquery untuk button hapus */
				
		/* Fungsi jquery untuk menyimpan hasil hapus */
		$('#modal-input').on('submit','#form-menuAkses-delete', function() {
			$.ajax({
				url: 'menuAkses/hapus_simpan.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(data) {
					$("#modal-input").modal('hide');
					alert("Data MenuAkses Berhasil Dihapus");
					loadDataMenuAkses();
				}
			});
			return false;
		});
		/* Akhir fungsi jquery untuk menyimpan hasil hapus */
		
	});
	/* Akhir fungsi yang dijalankan setelah halaman siap */
	
	
	
</script>


