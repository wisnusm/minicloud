<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-menu-add">
	<div class="modal-header"><h3>Menu Tambah</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Menu</label>
			<div class="col-md-6">
				<input type="text" id="kodeMenu" name="kodeMenu" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Menu</label>
			<div class="col-md-6">
				<input type="text" id="namaMenu" name="namaMenu" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Controller</label>
			<div class="col-md-6">
				<input type="text" id="controller" name="controller" />
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();" class="btn btn-primary">Simpan</button>
		</div>	
	</div>
</form>


<script type="text/javascript">
	function validasi() {
		var kodeMenu = document.getElementById('kodeMenu');
		var namaMenu = document.getElementById('namaMenu');
	 	var controller = document.getElementById('controller');		
		
		var hasil = true;

		if (kodeMenu.value == "") {
			kodeMenu.style.borderColor = "red";
			alert('Kode Menu kosong!');
			hasil = false;
		} else {
			kodeMenu.style.borderColor = "none";
		}

		if (namaMenu.value == "") {
			namaMenu.style.borderColor = "red";
			alert('Nama Menu kosong!');
			hasil = false;
		} else {
			namaMenu.style.borderColor = "none";
		}

		
		
		if (controller.value == "") {
			controller.style.borderColor = "red";
			alert('Controller kosong!');
			hasil = false;
		} else {
			controller.style.borderColor = "none";
		}
			
		
		
		return hasil;
	}
</script>