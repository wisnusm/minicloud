<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-menu-delete">
	<div class="modal-header"><h3>Menu Hapus</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Menu</label>
			<div class="col-md-6">
				<input type="hidden" id="kodeMenu" name="kodeMenu" value="${menuModel.kodeMenu}"/>
				<input type="text" id="kodeMenuDisplay" name="kodeMenuDisplay" value="${menuModel.kodeMenu}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Menu</label>
			<div class="col-md-6">
				<input type="text" id="namaMenu" name="namaMenu" value="${menuModel.namaMenu}" disabled="disabled"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Controller</label>
			<div class="col-md-6">
				<input type="text" id="controller" name="controller" value="${menuModel.controller}" disabled="disabled"/>
			</div>
		</div>
		<div class="modal-footer">
			Apakah anda yakin menghapus data ini? 
			<button type="submit" class="btn btn-danger">Yakin</button>
		</div>		
	</div>
</form>
