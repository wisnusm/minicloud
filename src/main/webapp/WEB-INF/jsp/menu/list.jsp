<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${menuModelList}" var="menuModel">
	<tr>
		<td>${menuModel.kodeMenu}</td>
		<td>${menuModel.namaMenu}</td>
		<td>
			<button type="button" id="btn-edit" value="${menuModel.kodeMenu}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${menuModel.kodeMenu}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${menuModel.kodeMenu}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>