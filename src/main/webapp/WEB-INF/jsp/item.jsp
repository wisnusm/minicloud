<% request.setAttribute("contextName", request.getContextPath()); %>

<div class="panel" style="background: white; margin-top: 40px; min-height: 620px">

	<div class="box-header">
		<br/>
		<h1 class="box-title">Halaman Item</h1>	
		<button type="button" id="btn-tambah" class="btn btn-primary pull-right">Tambah</button>
		
	</div>
	
	<div class="box-body">
	
		<div>
			<form action="#" method="get" id="form-item-search">
				<input type="text" name="keyword" id="keyword" class="form-input"/>
				<select name="tipe" id="tipe" class="form-input">
					<option value="kodeItem">Kode Item</option>
					<option value="namaItem">Nama Item</option>
				</select>
				<button type="submit" class="btn btn-warning">Cari</button>
			</form>			
		</div>
		
		<table class="table" id="tbl-item">
			<thead>
				<tr>
					<th>Kode Item</th>
					<th>Nama Item</th>
					<th>Kategori Item</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data-item">
			
			</tbody>
		</table>
	
	</div>

	
	<!-- div yang idnya modal-input itu untuk popup -->
	<div id="modal-input" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content"> 
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<h4>Form Menu Item</h4>
				</div>
				<div class="modal-body">
					<!-- div yg class modal-body itu isinya adalah jsp yang akan dishow -->
					
				</div>
			</div>
		</div>
	</div>
	
</div>

<script>

	loadDataItem(); 

	/* fungsi javascript untuk ngeload data dari table item */
	function loadDataItem() {
		/* ajax untuk load data */
		$.ajax({
			url:'item/list.html',
			type:'get',
			dataType:'html',
			success: function(result) {
				$('#list-data-item').html(result);
			}
		});
		/* fungsi ajax untuk load data */
	}
	/* Akhir fungsi javascript untuk ngeload data dari table item */

	/* Document ready itu, setelah halaman siap, function dijalankan */
	$(document).ready(function() {
		
		/* fungsi jquery untuk cari data */
		$('#form-item-search').on('submit', function() {	
			var keyword = document.getElementById('keyword').value;
			var tipe = document.getElementById('tipe').value;
			
			$.ajax({
				url:'item/cari.html',
				type:'get', 
				dataType:'html',  
				data:{keywordCari:keyword, tipeCari:tipe}, 
				success: function(result){
					$('#list-data-item').html(result);
				}
			});
			return false;
		});
		/* fungsi jquery untuk cari data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit','#form-item-delete', function() {	
			$.ajax({
				url:'item/simpan_delete.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data item terhapus!");
					loadDataItem(); 
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save edit data */
		$('#modal-input').on('submit','#form-item-edit', function() {	
			$.ajax({
				url:'item/simpan_edit.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data item terubah!");
					loadDataItem(); 
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save edit data */
		
		/* fungsi jquery untuk save data */
		$('#modal-input').on('submit','#form-item-add', function() {	
			$.ajax({
				url:'item/simpan_add.json', 
				type:'get', 
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$("#modal-input").modal('hide'); 
					alert("Data item tersimpan!");
					loadDataItem(); 
				}
			});
			return false; 
		});
		/* fungsi jquery untuk save data */
		
		/* fungsi jquery pop up detail */
		$('#list-data-item').on('click','#btn-detail', function() {	
			var kodeItemBtnDetail = $(this).val(); 
			$.ajax({
				url:'item/detail.html', 
				type:'get', 
				dataType:'html',
				data:{kodeItem:kodeItemBtnDetail},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery pop up delete */
		$('#list-data-item').on('click','#btn-delete', function() {	
			var kodeItemBtnDelete = $(this).val(); 
			$.ajax({
				url:'item/delete.html', 
				type:'get', 
				dataType:'html',
				data:{kodeItem:kodeItemBtnDelete},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery pop up edit */
		$('#list-data-item').on('click','#btn-edit', function() {
			var kodeItemBtnEdit = $(this).val(); 
			$.ajax({
				url:'item/edit.html', 
				type:'get', 
				dataType:'html',
				data:{kodeItem:kodeItemBtnEdit},
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');	
				}
			});
		});
		/*fungsi jquery pop up edit */
		
		/* fungsi jquery utk pengganti onclick di javascript */
		$('#btn-tambah').on('click', function() {			
			$.ajax({
				url:'item/tambah.html', 
				type:'get', 
				dataType:'html',
				success: function(result){
					$("#modal-input").find(".modal-body").html(result);
					$("#modal-input").modal('show');					
				}
			});
		});
		/* Akhir fungsi jquery utk pengganti onclick di javascript */
		
	});
	/* Akhir Document ready itu, setelah halaman siap, function dijalankan */
	
</script>