<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<c:forEach items="${departemenModelList}" var="departemenModel">
	<tr>
		<td>${departemenModel.kodeDepartemen}</td>
		<td>${departemenModel.namaDepartemen}</td>
		<td>${departemenModel.telpDepartemen}</td>
		<td>
			<button type="button" id="btn-edit" value="${departemenModel.kodeDepartemen}" class="btn btn-success">Ubah</button>
			<button type="button" id="btn-detail" value="${departemenModel.kodeDepartemen}" class="btn btn-warning">Rincian</button>
			<button type="button" id="btn-delete" value="${departemenModel.kodeDepartemen}" class="btn btn-danger">Hapus</button>
		</td>
	</tr>
</c:forEach>