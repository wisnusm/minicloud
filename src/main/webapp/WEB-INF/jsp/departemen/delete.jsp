<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-departemen-delete">
	<div class="modal-header"><h3>Menu Hapus</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Departemen</label>
			<div class="col-md-6">
				<input type="hidden" id="kodeDepartemen" name="kodeDepartemen" value="${departemenModel.kodeDepartemen}"/>
				<input type="text" id="kodeDepartemenDisplay" name="kodeDepartemenDisplay" value="${departemenModel.kodeDepartemen}" disabled="disabled"/>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Departemen</label>
			<div class="col-md-6">
				<input type="text" id="namaDepartemen" name="namaDepartemen" value="${departemenModel.namaDepartemen}" disabled="disabled"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Telepon Departemen</label>
			<div class="col-md-6">
				<input type="text" id="telpDepartemen" name="telpDepartemen" value="${departemenModel.telpDepartemen}" disabled="disabled"/>
			</div>
		</div>
		<div class="modal-footer">
			Apakah anda yakin menghapus data ini? 
			<button type="submit" class="btn btn-danger">Yakin</button>
		</div>		
	</div>
</form>
