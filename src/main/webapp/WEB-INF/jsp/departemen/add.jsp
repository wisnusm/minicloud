<%
	request.setAttribute("contextName", request.getContextPath());
%>

<form action="#" method="get" id="form-departemen-add">
	<div class="modal-header"><h3>Menu Tambah</h3></div>
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-3">Kode Departemen</label>
			<div class="col-md-6">
				<input type="text" id="kodeDepartemen" name="kodeDepartemen" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Departemen</label>
			<div class="col-md-6">
				<input type="text" id="namaDepartemen" name="namaDepartemen" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Telepon Departemen</label>
			<div class="col-md-6">
				<input type="text" id="telpDepartemen" name="telpDepartemen" />
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" onclick="return validasi();" class="btn btn-primary">Simpan</button>
		</div>	
	</div>
</form>


<script type="text/javascript">
	function validasi() {
		var kodeDepartemen = document.getElementById('kodeDepartemen');
		var namaDepartemen = document.getElementById('namaDepartemen');
	 	var telpDepartemen = document.getElementById('telpDepartemen');		
		
		var hasil = true;

		if (kodeDepartemen.value == "") {
			kodeDepartemen.style.borderColor = "red";
			alert('Kode Departemen kosong!');
			hasil = false;
		} else {
			kodeDepartemen.style.borderColor = "none";
		}

		if (namaDepartemen.value == "") {
			namaDepartemen.style.borderColor = "red";
			alert('Nama Departemen kosong!');
			hasil = false;
		} else {
			namaDepartemen.style.borderColor = "none";
		}

		
		
		if (telpDepartemen.value == "") {
			telpDepartemen.style.borderColor = "red";
			alert('Telepon Departemen kosong!');
			hasil = false;

		} else {
			// cek lagi apakah isinya angka atau mengandung huruf
			if (isNaN(telpDepartemen.value)) {
				// isNan maksudnya apakah nilainya Not a Number?				
				telpDepartemen.style.borderColor = "red";
				alert('Nomor telepon tidak sesuai format!');
				hasil = false;
			} else {
				// jika nilainya angka semua
				telpDepartemen.style.borderColor = "none";
			}
		}
			
		
		
		return hasil;
	}
</script>