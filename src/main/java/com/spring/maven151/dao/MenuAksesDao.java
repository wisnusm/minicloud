package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.MenuAksesModel;

public interface MenuAksesDao {

	/*query list data*/
	public List<MenuAksesModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(MenuAksesModel menuAksesModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public MenuAksesModel cariKode(String kodeMenuAkses) throws Exception;
	
	/*query update data*/
	public void update(MenuAksesModel menuAksesModel) throws Exception;
	
	/*query hapus data*/
	public void delete(MenuAksesModel menuAksesModel) throws Exception;
}
