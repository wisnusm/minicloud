package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.KategoriModel;

public interface KategoriDao {
	
	/*query simpan data*/
	public void create(KategoriModel kategoriModel) throws Exception;

	/*query list data*/
	public List<KategoriModel> list() throws Exception;
	
	public List<KategoriModel> selectKodeOrNama(String kodeKategori,String namaKategori);
	
	public List<KategoriModel> cariKategori(String keywordCari, String tipeCari);
	
	public KategoriModel cariKode(String kodeKategori) throws Exception;
	
	public void update(KategoriModel kategoriModel) throws Exception;
	
	public void delete(KategoriModel kategoriModel) throws Exception;
}
