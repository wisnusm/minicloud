package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.MenuDao;
import com.spring.maven151.model.MenuModel;

@SuppressWarnings("deprecation")
@Repository
public class MenuDaoImpl implements MenuDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<MenuModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		menuModelList = session.createQuery(" from MenuModel ").list();
		return menuModelList;
	}

	@Override
	public void create(MenuModel menuModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(menuModel);		
	}

	@Override
	public MenuModel cariKode(String kodeMenu) throws Exception {
		MenuModel menuModel = new MenuModel();
		Session session = this.sessionFactory.getCurrentSession();
		menuModel = (MenuModel) session.createQuery(" from MenuModel where kodeMenu = '"+kodeMenu+"' ").getSingleResult();
		return menuModel;
	}

	@Override
	public void update(MenuModel menuModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(menuModel);
	}

	@Override
	public void delete(MenuModel menuModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(menuModel);
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<MenuModel> getAllMenuTreeByRole(String kodeRole) throws Exception {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				String sqlquery = " SELECT "
						+ "		M.KODE_MENU KODE_MENU,	"
						+ " 	M.CONTROLLER CONTROLLER,	"	
						+ "		M.NAMA_MENU NAMA_MENU	"
						+ "	FROM M_MENU M "	
						+ "	JOIN M_MENU_AKSES MA "
						+ "		ON MA.KODE_MENU = M.KODE_MENU "	
						+ "	WHERE MA.KODE_ROLE = '"+kodeRole+"' "
						+ " ORDER BY M.NAMA_MENU ASC " ;
				
				Session session = sessionFactory.getCurrentSession();
				SQLQuery query = session.createSQLQuery(sqlquery);
				query.addEntity(MenuModel.class);
				List<MenuModel> result = query.list();
				return result;
	}

}
