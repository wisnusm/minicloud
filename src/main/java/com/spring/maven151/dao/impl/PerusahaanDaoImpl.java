package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.PerusahaanDao;
import com.spring.maven151.model.PerusahaanModel;

@Repository
public class PerusahaanDaoImpl implements PerusahaanDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<PerusahaanModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();
		perusahaanModelList = session.createQuery(" from PerusahaanModel ").list();
		return perusahaanModelList;
	}

	@Override
	public void create(PerusahaanModel perusahaanModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(perusahaanModel);		
	}

	@Override
	public PerusahaanModel cariKode(String kodePerusahaan) throws Exception {
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		Session session = this.sessionFactory.getCurrentSession();
		perusahaanModel = (PerusahaanModel) session.createQuery(" from PerusahaanModel where kodePerusahaan = '"+kodePerusahaan+"' ").getSingleResult();
		return perusahaanModel;
	}

	@Override
	public void update(PerusahaanModel perusahaanModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(perusahaanModel);
	}

	@Override
	public void delete(PerusahaanModel perusahaanModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(perusahaanModel);
		
	}

}
