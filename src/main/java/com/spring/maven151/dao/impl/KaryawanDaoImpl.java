package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.KaryawanDao;
import com.spring.maven151.model.KaryawanModel;

@Repository
public class KaryawanDaoImpl implements KaryawanDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<KaryawanModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
		karyawanModelList = session.createQuery(" from KaryawanModel ").list();
		return karyawanModelList;
	}

	@Override
	public void create(KaryawanModel karyawanModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(karyawanModel);		
	}

	@Override
	public KaryawanModel cariKode(String nik) throws Exception {
		KaryawanModel karyawanModel = new KaryawanModel();
		Session session = this.sessionFactory.getCurrentSession();
		karyawanModel = (KaryawanModel) session.createQuery(" from KaryawanModel where nik = '"+nik+"' ").getSingleResult();
		return karyawanModel;
	}

	@Override
	public void update(KaryawanModel karyawanModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(karyawanModel);
	}

	@Override
	public void delete(KaryawanModel karyawanModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(karyawanModel);
		
	}

}
