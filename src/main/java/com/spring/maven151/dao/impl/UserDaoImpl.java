package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.UserDao;
import com.spring.maven151.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<UserModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> userModelList = new ArrayList<UserModel>();
		userModelList = session.createQuery(" from UserModel ").list();
		return userModelList;
	}

	@Override
	public void create(UserModel userModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(userModel);		
	}

	@Override
	public UserModel cariKode(String kodeUser) throws Exception {
		UserModel userModel = new UserModel();
		Session session = this.sessionFactory.getCurrentSession();
		userModel = (UserModel) session.createQuery(" from UserModel where kodeUser = '"+kodeUser+"' ").getSingleResult();
		return userModel;
	}

	@Override
	public void update(UserModel userModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userModel);
	}

	@Override
	public void delete(UserModel userModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(userModel);
		
	}

	@Override
	public UserModel searchByUsernamePassword(String username, String password) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		UserModel result = null;
		try {
			result = (UserModel) session.createQuery(" from UserModel "
													+ " where username = '"+username+"' "
													+ " and password = '"+password+"' ").getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println();
		}
		return result;
	}

}
