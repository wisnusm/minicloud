package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.DepartemenDao;
import com.spring.maven151.model.DepartemenModel;

@Repository
public class DepartemenDaoImpl implements DepartemenDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<DepartemenModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();
		departemenModelList = session.createQuery(" from DepartemenModel ").list();
		return departemenModelList;
	}

	@Override
	public void create(DepartemenModel departemenModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(departemenModel);		
	}

	@Override
	public DepartemenModel cariKode(String kodeDepartemen) throws Exception {
		DepartemenModel departemenModel = new DepartemenModel();
		Session session = this.sessionFactory.getCurrentSession();
		departemenModel = (DepartemenModel) session.createQuery(" from DepartemenModel where kodeDepartemen = '"+kodeDepartemen+"' ").getSingleResult();
		return departemenModel;
	}

	@Override
	public void update(DepartemenModel departemenModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(departemenModel);
	}

	@Override
	public void delete(DepartemenModel departemenModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(departemenModel);
		
	}

}
