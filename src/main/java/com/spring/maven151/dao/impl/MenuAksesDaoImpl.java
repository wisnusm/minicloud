package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.MenuAksesDao;
import com.spring.maven151.model.MenuAksesModel;

@Repository
public class MenuAksesDaoImpl implements MenuAksesDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<MenuAksesModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuAksesModel> menuAksesModelList = new ArrayList<MenuAksesModel>();
		menuAksesModelList = session.createQuery(" from MenuAksesModel ").list();
		return menuAksesModelList;
	}

	@Override
	public void create(MenuAksesModel menuAksesModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(menuAksesModel);		
	}

	@Override
	public MenuAksesModel cariKode(String kodeMenuAkses) throws Exception {
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		Session session = this.sessionFactory.getCurrentSession();
		menuAksesModel = (MenuAksesModel) session.createQuery(" from MenuAksesModel where kodeMenuAkses = '"+kodeMenuAkses+"' ").getSingleResult();
		return menuAksesModel;
	}

	@Override
	public void update(MenuAksesModel menuAksesModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(menuAksesModel);
	}

	@Override
	public void delete(MenuAksesModel menuAksesModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(menuAksesModel);
		
	}

}
