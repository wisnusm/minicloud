package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.KategoriDao;
import com.spring.maven151.model.KategoriModel;

@Repository
public class KategoriDaoImpl implements KategoriDao{
	
	@Autowired
	private SessionFactory sessionFactory; /*sessionFactory itu utk konek ke applicationContext.xml*/

	@Override
	public void create(KategoriModel kategoriModel) throws Exception {
		// TODO Auto-generated method stub
		
		/*session itu sintaks awal utk queri hibernatenya*/
		Session session = this.sessionFactory.getCurrentSession(); 
		
		 /*session.save(object) itu fungsi hibernate tuk query insert into*/
		session.save(kategoriModel); 
		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KategoriModel> list() throws Exception {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession(); 
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		
		/*session.createQuery(string query) itu fungsi hibernate utk query select * from Model*/
		kategoriModelList = session.createQuery(" from KategoriModel ").list();
		
		return kategoriModelList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KategoriModel> selectKodeOrNama(String kodeKategori, String namaKategori) {
		// TODO Auto-generated method stub
		
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		Session session = this.sessionFactory.getCurrentSession(); 
		kategoriModelList = session.createQuery(" from KategoriModel "
											  + " where kodeKategori !='"+kodeKategori+"' "
											  + "      or namaKategori !='"+namaKategori+"' ").list();
		
		return kategoriModelList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KategoriModel> cariKategori(String keywordCari, String tipeCari) {
		// TODO Auto-generated method stub
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		String condition =" ";
		
		if (tipeCari.equals("kodeKategori")) {
			condition =" where kodeKategori like '%"+keywordCari+"%' ";
			
		}else if (tipeCari.equals("namaKategori")) {
			condition =" where namaKategori like '%"+keywordCari+"%' ";
			
		}else {
			
		}
		
		Session session = this.sessionFactory.getCurrentSession(); 
		kategoriModelList = session.createQuery(" from KategoriModel "+condition).list();
		return kategoriModelList;
	}

	@Override
	public KategoriModel cariKode(String kodeKategori) throws Exception {
		// TODO Auto-generated method stub
		KategoriModel kategoriModel = new KategoriModel();
		Session session = this.sessionFactory.getCurrentSession(); 
		kategoriModel = (KategoriModel) session.createQuery(" from KategoriModel "
									      + " where kodeKategori ='"+kodeKategori+"' ").getSingleResult();
		return kategoriModel;
	}

	@Override
	public void update(KategoriModel kategoriModel) throws Exception {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession(); 
		
		 /*session.update(object) itu fungsi hibernate tuk query update table set kolom*/
		session.update(kategoriModel);
	}

	@Override
	public void delete(KategoriModel kategoriModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		
		 /*session.delete(object) itu fungsi hibernate tuk query delete data*/
		session.delete(kategoriModel);
		
	}

}
