package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.SupplierDao;
import com.spring.maven151.model.SequenceModel;
import com.spring.maven151.model.SupplierModel;

@Repository
public class SupplierDaoImpl implements SupplierDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(SupplierModel supplierModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		session.save(supplierModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierModel> list() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		supplierModelList = session.createQuery(" from SupplierModel ").list();
		
		return supplierModelList;
	}

	@Override
	public List<SupplierModel> cariSupplier(String keywordCari, String tipeCari) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SupplierModel cariKode(String kodeSupplier) throws Exception {
		// TODO Auto-generated method stub
		SupplierModel supplierModel = null;
		Session session = this.sessionFactory.getCurrentSession(); 
		try {
			supplierModel = (SupplierModel) session.createQuery(" from SupplierModel "
    				  + " where kodeSupplier ='"+kodeSupplier+"' ").getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return supplierModel;
	}

	@Override
	public void update(SupplierModel supplierModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		session.update(supplierModel);
	}

	@Override
	public void delete(SupplierModel supplierModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		session.delete(supplierModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierModel> listIsNotDelete() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		supplierModelList = session.createQuery(" from SupplierModel where  xIsDeleteSupplier = 0 ").list();
		
		return supplierModelList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierModel> cariNamaSupplier(String namaSupplier) throws Exception {
		// TODO Auto-generated method stub
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		Session session = this.sessionFactory.getCurrentSession(); 
		try {
			supplierModelList = session.createQuery(" from SupplierModel "
												+ " where namaSupplier ='"+namaSupplier+"' ").list();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return supplierModelList;
	}

	@Override
	public Integer sequenceValueSupplier() throws Exception {
		// TODO Auto-generated method stub
		
		SequenceModel sequenceModel = null;
		Integer seqIdSupplier = 0;
		Session session = this.sessionFactory.getCurrentSession(); 
		try {
			sequenceModel = (SequenceModel) session.createQuery(" from SequenceModel "
    				  					 + " where sequenceName = 'M_ID_SUPPLIER' ").getSingleResult();
			seqIdSupplier = sequenceModel.getSequenceValue();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return seqIdSupplier;
	} 
}
