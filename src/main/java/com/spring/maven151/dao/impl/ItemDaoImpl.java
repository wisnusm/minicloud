package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.ItemDao;
import com.spring.maven151.model.ItemModel;

@Repository
public class ItemDaoImpl implements ItemDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(ItemModel itemModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		session.save(itemModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemModel> list() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		List<ItemModel> itemModelList = new ArrayList<ItemModel>();
		itemModelList = session.createQuery(" from ItemModel ").list();
		
		return itemModelList;
	}

	@Override
	public List<ItemModel> cariItem(String keywordCari, String tipeCari) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemModel cariKode(String kodeItem) throws Exception {
		// TODO Auto-generated method stub
		ItemModel itemModel = new ItemModel();
		Session session = this.sessionFactory.getCurrentSession(); 
		itemModel = (ItemModel) session.createQuery(" from ItemModel "
									      + " where kodeItem ='"+kodeItem+"' ").getSingleResult();
		return itemModel;
	}

	@Override
	public void update(ItemModel itemModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		session.update(itemModel);
	}

	@Override
	public void delete(ItemModel itemModel) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		session.delete(itemModel);
	} 
}
