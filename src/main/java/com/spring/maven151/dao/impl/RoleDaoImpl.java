package com.spring.maven151.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.maven151.dao.RoleDao;
import com.spring.maven151.model.RoleModel;

@Repository
public class RoleDaoImpl implements RoleDao{

	@Autowired
	private SessionFactory sessionFactory;	
	
	@SuppressWarnings("unchecked")
	public List<RoleModel> list() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = session.createQuery(" from RoleModel ").list();
		return roleModelList;
	}

	@Override
	public void create(RoleModel roleModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(roleModel);		
	}

	@Override
	public RoleModel cariKode(String kodeRole) throws Exception {
		RoleModel roleModel = new RoleModel();
		Session session = this.sessionFactory.getCurrentSession();
		roleModel = (RoleModel) session.createQuery(" from RoleModel where kodeRole = '"+kodeRole+"' ").getSingleResult();
		return roleModel;
	}

	@Override
	public void update(RoleModel roleModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(roleModel);
	}

	@Override
	public void delete(RoleModel roleModel) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(roleModel);
		
	}

}
