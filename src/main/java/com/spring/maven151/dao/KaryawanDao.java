package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.KaryawanModel;

public interface KaryawanDao {

	/*query list data*/
	public List<KaryawanModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(KaryawanModel karyawanModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public KaryawanModel cariKode(String nik) throws Exception;
	
	/*query update data*/
	public void update(KaryawanModel karyawanModel) throws Exception;
	
	/*query hapus data*/
	public void delete(KaryawanModel karyawanModel) throws Exception;
}
