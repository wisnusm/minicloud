package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.MenuModel;

public interface MenuDao {

	/*query list data*/
	public List<MenuModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(MenuModel menuModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public MenuModel cariKode(String kodeMenu) throws Exception;
	
	/*query update data*/
	public void update(MenuModel menuModel) throws Exception;
	
	/*query hapus data*/
	public void delete(MenuModel menuModel) throws Exception;
	
	public List<MenuModel> getAllMenuTreeByRole(String kodeRole) throws Exception;
}
