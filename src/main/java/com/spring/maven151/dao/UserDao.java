package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.UserModel;

public interface UserDao {

	/*query list data*/
	public List<UserModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(UserModel userModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public UserModel cariKode(String kodeUser) throws Exception;
	
	/*query update data*/
	public void update(UserModel userModel) throws Exception;
	
	/*query hapus data*/
	public void delete(UserModel userModel) throws Exception;
	
	public UserModel searchByUsernamePassword(String username, String password) throws Exception;
}
