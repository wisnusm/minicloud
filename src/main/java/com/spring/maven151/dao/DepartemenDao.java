package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.DepartemenModel;

public interface DepartemenDao {

	/*query list data*/
	public List<DepartemenModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(DepartemenModel departemenModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public DepartemenModel cariKode(String kodeDepartemen) throws Exception;
	
	/*query update data*/
	public void update(DepartemenModel departemenModel) throws Exception;
	
	/*query hapus data*/
	public void delete(DepartemenModel departemenModel) throws Exception;
}
