package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.RoleModel;

public interface RoleDao {

	/*query list data*/
	public List<RoleModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(RoleModel roleModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public RoleModel cariKode(String kodeRole) throws Exception;
	
	/*query update data*/
	public void update(RoleModel roleModel) throws Exception;
	
	/*query hapus data*/
	public void delete(RoleModel roleModel) throws Exception;
}
