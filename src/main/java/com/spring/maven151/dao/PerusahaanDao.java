package com.spring.maven151.dao;

import java.util.List;

import com.spring.maven151.model.PerusahaanModel;

public interface PerusahaanDao {

	/*query list data*/
	public List<PerusahaanModel> list() throws Exception;
	
	/*query simpan data*/
	public void create(PerusahaanModel perusahaanModel) throws Exception;
	
	/*query cari data berdasarkan kode*/
	public PerusahaanModel cariKode(String kodePerusahaan) throws Exception;
	
	/*query update data*/
	public void update(PerusahaanModel perusahaanModel) throws Exception;
	
	/*query hapus data*/
	public void delete(PerusahaanModel perusahaanModel) throws Exception;
}
