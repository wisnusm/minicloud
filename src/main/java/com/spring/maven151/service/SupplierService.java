package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.SupplierModel;

public interface SupplierService {
	
	public void create(SupplierModel supplierModel) throws Exception;
	
	public List<SupplierModel> list() throws Exception;
	
	public List<SupplierModel> cariSupplier(String keywordCari, String tipeCari) throws Exception;
	
	public SupplierModel cariKode(String kodeSupplier) throws Exception;
	
	public void update(SupplierModel supplierModel) throws Exception;
	
	public void delete(SupplierModel supplierModel) throws Exception;
	
	public List<SupplierModel> listIsNotDelete() throws Exception;
	
	// public output namaMethod(input)
	public List<SupplierModel> cariNamaSupplier(String namaSupplier) throws Exception;
	
	public Integer sequenceValueSupplier() throws Exception;

}
