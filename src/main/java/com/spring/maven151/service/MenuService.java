package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.MenuModel;

public interface MenuService {
	/*transaksi list data*/
	public List<MenuModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(MenuModel menuModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public MenuModel cariKode(String kodeMenu) throws Exception;
	
	/*transaksi update data*/
	public void update(MenuModel menuModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(MenuModel menuModel) throws Exception;
	
	public List<MenuModel> getAllMenuTreeByRole(String kodeRole) throws Exception;
}
