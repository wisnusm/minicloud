package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.SupplierDao;
import com.spring.maven151.model.SupplierModel;
import com.spring.maven151.service.SupplierService;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService{

	@Autowired /*autowired itu utk konek kategoriDao*/
	private SupplierDao supplierDao;
	
	@Override
	public void create(SupplierModel supplierModel) throws Exception {
		// TODO Auto-generated method stub
		this.supplierDao.create(supplierModel);
	}

	@Override
	public List<SupplierModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.supplierDao.list();
	}

	@Override
	public List<SupplierModel> cariSupplier(String keywordCari, String tipeCari) throws Exception {
		// TODO Auto-generated method stub
		return this.supplierDao.cariSupplier(keywordCari, tipeCari);
	}

	@Override
	public SupplierModel cariKode(String kodeSupplier) throws Exception {
		// TODO Auto-generated method stub
		return this.supplierDao.cariKode(kodeSupplier);
	}

	@Override
	public void update(SupplierModel supplierModel) throws Exception {
		// TODO Auto-generated method stub
		this.supplierDao.update(supplierModel);
	}

	@Override
	public void delete(SupplierModel supplierModel) throws Exception {
		// TODO Auto-generated method stub
		this.supplierDao.delete(supplierModel);
	}

	@Override
	public List<SupplierModel> listIsNotDelete() throws Exception {
		// TODO Auto-generated method stub
		return this.supplierDao.listIsNotDelete();
	}

	@Override
	public List<SupplierModel> cariNamaSupplier(String namaSupplier) throws Exception {
		// TODO Auto-generated method stub
		return this.supplierDao.cariNamaSupplier(namaSupplier);
	}

	@Override
	public Integer sequenceValueSupplier() throws Exception {
		// TODO Auto-generated method stub
		return this.supplierDao.sequenceValueSupplier();
	}

}
