package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.DepartemenDao;
import com.spring.maven151.model.DepartemenModel;
import com.spring.maven151.service.DepartemenService;

@Service
@Transactional
public class DepartemenServiceImpl implements DepartemenService {
	
	@Autowired
	private DepartemenDao departemenDao;
	
	@Override
	public List<DepartemenModel> list() throws Exception {
		return this.departemenDao.list();
	}

	@Override
	public void create(DepartemenModel departemenModel) throws Exception {
		this.departemenDao.create(departemenModel);
	}

	@Override
	public DepartemenModel cariKode(String kodeDepartemen) throws Exception {
		return this.departemenDao.cariKode(kodeDepartemen);
	}

	@Override
	public void update(DepartemenModel departemenModel) throws Exception {
		this.departemenDao.update(departemenModel);
	}

	@Override
	public void delete(DepartemenModel departemenModel) throws Exception {
		this.departemenDao.delete(departemenModel);
		
	}

}
