package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.ItemDao;
import com.spring.maven151.model.ItemModel;
import com.spring.maven151.service.ItemService;

@Service
@Transactional
public class ItemServiceImpl implements ItemService{

	@Autowired /*autowired itu utk konek kategoriDao*/
	private ItemDao itemDao;
	
	@Override
	public void create(ItemModel itemModel) throws Exception {
		// TODO Auto-generated method stub
		this.itemDao.create(itemModel);
	}

	@Override
	public List<ItemModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.itemDao.list();
	}

	@Override
	public List<ItemModel> cariItem(String keywordCari, String tipeCari) throws Exception {
		// TODO Auto-generated method stub
		return this.itemDao.cariItem(keywordCari, tipeCari);
	}

	@Override
	public ItemModel cariKode(String kodeItem) throws Exception {
		// TODO Auto-generated method stub
		return this.itemDao.cariKode(kodeItem);
	}

	@Override
	public void update(ItemModel itemModel) throws Exception {
		// TODO Auto-generated method stub
		this.itemDao.update(itemModel);
	}

	@Override
	public void delete(ItemModel itemModel) throws Exception {
		// TODO Auto-generated method stub
		this.itemDao.delete(itemModel);
	}

}
