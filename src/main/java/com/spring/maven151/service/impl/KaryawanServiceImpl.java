package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.KaryawanDao;
import com.spring.maven151.model.KaryawanModel;
import com.spring.maven151.service.KaryawanService;

@Service
@Transactional
public class KaryawanServiceImpl implements KaryawanService {
	
	@Autowired
	private KaryawanDao karyawanDao;
	
	@Override
	public List<KaryawanModel> list() throws Exception {
		return this.karyawanDao.list();
	}

	@Override
	public void create(KaryawanModel karyawanModel) throws Exception {
		this.karyawanDao.create(karyawanModel);
	}

	@Override
	public KaryawanModel cariKode(String nik) throws Exception {
		return this.karyawanDao.cariKode(nik);
	}

	@Override
	public void update(KaryawanModel karyawanModel) throws Exception {
		this.karyawanDao.update(karyawanModel);
	}

	@Override
	public void delete(KaryawanModel karyawanModel) throws Exception {
		this.karyawanDao.delete(karyawanModel);
		
	}

}
