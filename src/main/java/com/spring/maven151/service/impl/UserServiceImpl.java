package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.UserDao;
import com.spring.maven151.model.UserModel;
import com.spring.maven151.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public List<UserModel> list() throws Exception {
		return this.userDao.list();
	}

	@Override
	public void create(UserModel userModel) throws Exception {
		this.userDao.create(userModel);
	}

	@Override
	public UserModel cariKode(String kodeUser) throws Exception {
		return this.userDao.cariKode(kodeUser);
	}

	@Override
	public void update(UserModel userModel) throws Exception {
		this.userDao.update(userModel);
	}

	@Override
	public void delete(UserModel userModel) throws Exception {
		this.userDao.delete(userModel);
		
	}

	@Override
	public UserModel searchByUsernamePassword(String username, String password) throws Exception {
		// TODO Auto-generated method stub
		return this.userDao.searchByUsernamePassword(username, password);
	}

}
