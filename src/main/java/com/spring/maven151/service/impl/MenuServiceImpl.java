package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.MenuDao;
import com.spring.maven151.model.MenuModel;
import com.spring.maven151.service.MenuService;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	private MenuDao menuDao;
	
	@Override
	public List<MenuModel> list() throws Exception {
		return this.menuDao.list();
	}

	@Override
	public void create(MenuModel menuModel) throws Exception {
		this.menuDao.create(menuModel);
	}

	@Override
	public MenuModel cariKode(String kodeMenu) throws Exception {
		return this.menuDao.cariKode(kodeMenu);
	}

	@Override
	public void update(MenuModel menuModel) throws Exception {
		this.menuDao.update(menuModel);
	}

	@Override
	public void delete(MenuModel menuModel) throws Exception {
		this.menuDao.delete(menuModel);
		
	}

	@Override
	public List<MenuModel> getAllMenuTreeByRole(String kodeRole) throws Exception {
		// TODO Auto-generated method stub
		return this.menuDao.getAllMenuTreeByRole(kodeRole);
	}

}
