package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.MenuAksesDao;
import com.spring.maven151.model.MenuAksesModel;
import com.spring.maven151.service.MenuAksesService;

@Service
@Transactional
public class MenuAksesServiceImpl implements MenuAksesService {
	
	@Autowired
	private MenuAksesDao menuAksesDao;
	
	@Override
	public List<MenuAksesModel> list() throws Exception {
		return this.menuAksesDao.list();
	}

	@Override
	public void create(MenuAksesModel menuAksesModel) throws Exception {
		this.menuAksesDao.create(menuAksesModel);
	}

	@Override
	public MenuAksesModel cariKode(String kodeMenuAkses) throws Exception {
		return this.menuAksesDao.cariKode(kodeMenuAkses);
	}

	@Override
	public void update(MenuAksesModel menuAksesModel) throws Exception {
		this.menuAksesDao.update(menuAksesModel);
	}

	@Override
	public void delete(MenuAksesModel menuAksesModel) throws Exception {
		this.menuAksesDao.delete(menuAksesModel);
		
	}

}
