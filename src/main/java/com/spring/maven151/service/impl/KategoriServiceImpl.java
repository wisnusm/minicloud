package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.KategoriDao;
import com.spring.maven151.model.KategoriModel;
import com.spring.maven151.service.KategoriService;

@Service
@Transactional
public class KategoriServiceImpl implements KategoriService {
	
	@Autowired /*autowired itu utk konek kategoriDao*/
	private KategoriDao kategoriDao;

	@Override
	public void create(KategoriModel kategoriModel) throws Exception {
		// TODO Auto-generated method stub
		this.kategoriDao.create(kategoriModel);
		
	}

	@Override
	public List<KategoriModel> list() throws Exception {
		// TODO Auto-generated method stub
		return this.kategoriDao.list();
	}

	@Override
	public List<KategoriModel> selectKodeOrNama(String kodeKategori, String namaKategori) {
		// TODO Auto-generated method stub
		return this.kategoriDao.selectKodeOrNama(kodeKategori, namaKategori);
	}

	@Override
	public List<KategoriModel> cariKategori(String keywordCari, String tipeCari) {
		// TODO Auto-generated method stub
		return this.kategoriDao.cariKategori(keywordCari, tipeCari);
	}

	@Override
	public KategoriModel cariKode(String kodeKategori) throws Exception {
		// TODO Auto-generated method stub
		return this.kategoriDao.cariKode(kodeKategori);
	}

	@Override
	public void update(KategoriModel kategoriModel) throws Exception {
		// TODO Auto-generated method stub
		this.kategoriDao.update(kategoriModel);
	}

	@Override
	public void delete(KategoriModel kategoriModel) throws Exception {
		// TODO Auto-generated method stub
		this.kategoriDao.delete(kategoriModel);
	}

}
