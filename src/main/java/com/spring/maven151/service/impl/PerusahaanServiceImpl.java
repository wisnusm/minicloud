package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.PerusahaanDao;
import com.spring.maven151.model.PerusahaanModel;
import com.spring.maven151.service.PerusahaanService;

@Service
@Transactional
public class PerusahaanServiceImpl implements PerusahaanService {
	
	@Autowired
	private PerusahaanDao perusahaanDao;
	
	@Override
	public List<PerusahaanModel> list() throws Exception {
		return this.perusahaanDao.list();
	}

	@Override
	public void create(PerusahaanModel perusahaanModel) throws Exception {
		this.perusahaanDao.create(perusahaanModel);
	}

	@Override
	public PerusahaanModel cariKode(String kodePerusahaan) throws Exception {
		return this.perusahaanDao.cariKode(kodePerusahaan);
	}

	@Override
	public void update(PerusahaanModel perusahaanModel) throws Exception {
		this.perusahaanDao.update(perusahaanModel);
	}

	@Override
	public void delete(PerusahaanModel perusahaanModel) throws Exception {
		this.perusahaanDao.delete(perusahaanModel);
		
	}

}
