package com.spring.maven151.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.maven151.dao.RoleDao;
import com.spring.maven151.model.RoleModel;
import com.spring.maven151.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<RoleModel> list() throws Exception {
		return this.roleDao.list();
	}

	@Override
	public void create(RoleModel roleModel) throws Exception {
		this.roleDao.create(roleModel);
	}

	@Override
	public RoleModel cariKode(String kodeRole) throws Exception {
		return this.roleDao.cariKode(kodeRole);
	}

	@Override
	public void update(RoleModel roleModel) throws Exception {
		this.roleDao.update(roleModel);
	}

	@Override
	public void delete(RoleModel roleModel) throws Exception {
		this.roleDao.delete(roleModel);
		
	}

}
