package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.ItemModel;

public interface ItemService {
	
	public void create(ItemModel itemModel) throws Exception;
	
	public List<ItemModel> list() throws Exception;
	
	public List<ItemModel> cariItem(String keywordCari, String tipeCari) throws Exception;
	
	public ItemModel cariKode(String kodeItem) throws Exception;
	
	public void update(ItemModel itemModel) throws Exception;
	
	public void delete(ItemModel itemModel) throws Exception;

}
