package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.KaryawanModel;

public interface KaryawanService {
	/*transaksi list data*/
	public List<KaryawanModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(KaryawanModel karyawanModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public KaryawanModel cariKode(String nik) throws Exception;
	
	/*transaksi update data*/
	public void update(KaryawanModel karyawanModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(KaryawanModel karyawanModel) throws Exception;
}
