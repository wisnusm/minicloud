package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.UserModel;

public interface UserService {
	/*transaksi list data*/
	public List<UserModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(UserModel userModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public UserModel cariKode(String kodeUser) throws Exception;
	
	/*transaksi update data*/
	public void update(UserModel userModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(UserModel userModel) throws Exception;
	
	public UserModel searchByUsernamePassword(String username, String password) throws Exception;
}
