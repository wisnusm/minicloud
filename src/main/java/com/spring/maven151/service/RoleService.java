package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.RoleModel;

public interface RoleService {
	/*transaksi list data*/
	public List<RoleModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(RoleModel roleModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public RoleModel cariKode(String kodeRole) throws Exception;
	
	/*transaksi update data*/
	public void update(RoleModel roleModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(RoleModel roleModel) throws Exception;
}
