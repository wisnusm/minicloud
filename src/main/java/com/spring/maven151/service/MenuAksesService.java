package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.MenuAksesModel;

public interface MenuAksesService {
	/*transaksi list data*/
	public List<MenuAksesModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(MenuAksesModel menuAksesModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public MenuAksesModel cariKode(String kodeMenuAkses) throws Exception;
	
	/*transaksi update data*/
	public void update(MenuAksesModel menuAksesModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(MenuAksesModel menuAksesModel) throws Exception;
}
