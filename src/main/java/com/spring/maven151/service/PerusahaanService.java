package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.PerusahaanModel;

public interface PerusahaanService {
	/*transaksi list data*/
	public List<PerusahaanModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(PerusahaanModel perusahaanModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public PerusahaanModel cariKode(String kodePerusahaan) throws Exception;
	
	/*transaksi update data*/
	public void update(PerusahaanModel perusahaanModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(PerusahaanModel perusahaanModel) throws Exception;
}
