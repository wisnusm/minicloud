package com.spring.maven151.service;

import java.util.List;

import com.spring.maven151.model.DepartemenModel;

public interface DepartemenService {
	/*transaksi list data*/
	public List<DepartemenModel> list() throws Exception;

	/*transaksi simpan data*/
	public void create(DepartemenModel departemenModel) throws Exception;

	/*transaksi cari data berdasarkan kode*/
	public DepartemenModel cariKode(String kodeDepartemen) throws Exception;
	
	/*transaksi update data*/
	public void update(DepartemenModel departemenModel) throws Exception;

	/*transaksi hapus data*/
	public void delete(DepartemenModel departemenModel) throws Exception;
}
