package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.KategoriModel;
import com.spring.maven151.service.KategoriService;

@Controller
public class KategoriController {
	
	@Autowired /*utk konek ke servicenya*/
	private KategoriService kategoriService;
	
	@RequestMapping(value="kategori")
	public String kategoriMethod(){
		String halaman = "kategori";
		return halaman;
	}
	
	@RequestMapping(value="kategori/cari")
	public String cariKategoriMethod(Model model, HttpServletRequest request){
		String halaman = "kategori/list";
		
		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");
		
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		try {
			kategoriModelList = this.kategoriService.cariKategori(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("kategoriModelList", kategoriModelList);	
		return halaman;
	}
	
	@RequestMapping(value="kategori/list")
	public String listKategoriMethod(Model model){
		String halaman = "kategori/list";
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();
		
		try {
			kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		model.addAttribute("kategoriModelList", kategoriModelList);
		return halaman;
	}	
	
	@RequestMapping(value="kategori/simpan_delete")
	public String simpanDeleteKategoriMethod(HttpServletRequest request){
		String halaman = "kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		
		KategoriModel kategoriModelDB = new KategoriModel();
		
		try {
			/*kita cari dulu data yg lama berdasarkan Kode/PrimaryKey-nya*/
			kategoriModelDB = this.kategoriService.cariKode(kodeKategori);
			
			this.kategoriService.delete(kategoriModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="kategori/simpan_edit")
	public String simpanEditKategoriMethod(HttpServletRequest request){
		String halaman = "kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		String namaKategori = request.getParameter("namaKategori");
		String keteranganKategori = request.getParameter("keteranganKategori");
		
		KategoriModel kategoriModelDB = new KategoriModel();
		
		try {
			/*kita cari dulu data yg lama berdasarkan Kode/PrimaryKey-nya*/
			kategoriModelDB = this.kategoriService.cariKode(kodeKategori);
			
			/*kita hanya menset nilai baru hanya utk kolom2 yg diubah saja*/
			kategoriModelDB.setNamaKategori(namaKategori);
			kategoriModelDB.setKeteranganKategori(keteranganKategori);
			
			this.kategoriService.update(kategoriModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}

	@RequestMapping(value="kategori/simpan_add")
	public String simpanAddKategoriMethod(HttpServletRequest request){
		String halaman = "kategori";
		
		String kodeKategori = request.getParameter("kodeKategori");
		String namaKategori = request.getParameter("namaKategori");
		String keteranganKategori = request.getParameter("keteranganKategori");
		
		KategoriModel kategoriModel = new KategoriModel(); 
		
		kategoriModel.setKodeKategori(kodeKategori);
		kategoriModel.setNamaKategori(namaKategori);
		kategoriModel.setKeteranganKategori(keteranganKategori);
		
		try {
			this.kategoriService.create(kategoriModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="kategori/sub_detail")
	public String subDetailKategoriMethod(){
		String halaman = "kategori/sub_detail";
		return halaman;
	}
	
	@RequestMapping(value="kategori/detail")
	public String detailKategoriMethod(Model model, HttpServletRequest request){
		String halaman = "kategori/detail";
		String kodeKategori = request.getParameter("kodeKategori");
		KategoriModel kategoriModel = new KategoriModel();
		try {
			kategoriModel = this.kategoriService.cariKode(kodeKategori);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("kategoriModel",kategoriModel);
		return halaman;
	}
	
	@RequestMapping(value="kategori/delete")
	public String deleteKategoriMethod(Model model, HttpServletRequest request){
		String halaman = "kategori/delete";
		String kodeKategori = request.getParameter("kodeKategori");
		KategoriModel kategoriModel = new KategoriModel();
		try {
			kategoriModel = this.kategoriService.cariKode(kodeKategori);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("kategoriModel",kategoriModel);
		return halaman;
	}
	
	@RequestMapping(value="kategori/edit")
	public String editKategoriMethod(Model model, HttpServletRequest request){
		String halaman = "kategori/edit";
		String kodeKategori = request.getParameter("kodeKategori");
		KategoriModel kategoriModel = new KategoriModel();
		try {
			kategoriModel = this.kategoriService.cariKode(kodeKategori);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("kategoriModel",kategoriModel);
		return halaman;
	}
	
	@RequestMapping(value="kategori/tambah")
	public String tambahKategoriMethod(){
		String halaman = "kategori/add";
		return halaman;
	}
	
	@RequestMapping(value="halaman_hasil_kategori")
	public String hasilKategoriMethod(HttpServletRequest request, Model model) {
		String halaman = "kategori/hasil_kategori";
		
		//get value dari name yaitu variable html di jspnya
		//HttpServletRequest request adalah function utk get value dari jsp
		String kodeKategori = request.getParameter("kodeKategori");
		String namaKategori = request.getParameter("namaKategori");
		String keteranganKategori = request.getParameter("keteranganKategori");
		
		//set value to variable $ di jsp hasil
		model.addAttribute("kodeKategori", kodeKategori);
		model.addAttribute("namaKategori", namaKategori);
		model.addAttribute("keteranganKategori", keteranganKategori);
		
		return halaman;
	}

}
