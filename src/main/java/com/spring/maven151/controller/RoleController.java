package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.RoleModel;
import com.spring.maven151.service.RoleService;

@Controller
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "role")
	public String roleMethod() {
		String halaman = "role";
		return halaman;
	}
	
	@RequestMapping(value ="role/list")
	public String listRoleMethod(Model model) {
		String halaman = "role/list";
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("roleModelList", roleModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="role/tambah")
	public String tambahRoleMethod() {
		String halaman = "role/add";
		return halaman;
	}
	
	@RequestMapping(value ="role/tambah_simpan")
	public String tambahSimpanRoleMethod(HttpServletRequest request) {
		String halaman = "role";
		
		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");
		
		RoleModel roleModel = new RoleModel();
		
		roleModel.setKodeRole(kodeRole);
		roleModel.setNamaRole(namaRole);
		
		try {
			this.roleService.create(roleModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "role/ubah")
	public String ubahRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/edit";
		String kodeRole = request.getParameter("kodeRole");
		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("roleModel",roleModel);
		return halaman;
	}
	
	@RequestMapping(value = "role/ubah_simpan")
	public String ubahSimpanRoleMethod(HttpServletRequest request) {
		String halaman = "role";
		String kodeRole = request.getParameter("kodeRole");
		String namaRole = request.getParameter("namaRole");
		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
			roleModel.setNamaRole(namaRole);
			this.roleService.update(roleModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "role/rincian")
	public String rincianRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/detail";
		String kodeRole = request.getParameter("kodeRole");
		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("roleModel",roleModel);
		return halaman;
	}
	
	@RequestMapping(value = "role/hapus")
	public String hapusRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role/delete";
		String kodeRole = request.getParameter("kodeRole");
		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("roleModel",roleModel);
		return halaman;
	}
	
	@RequestMapping(value = "role/hapus_simpan")
	public String hapusSimpanRoleMethod(HttpServletRequest request, Model model) {
		String halaman = "role";
		String kodeRole = request.getParameter("kodeRole");
		RoleModel roleModel = new RoleModel();
		try {
			roleModel = this.roleService.cariKode(kodeRole);
			this.roleService.delete(roleModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("roleModel",roleModel);
		return halaman;
	}
}
