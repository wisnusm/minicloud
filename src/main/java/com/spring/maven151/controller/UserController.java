package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.KaryawanModel;
import com.spring.maven151.model.RoleModel;
import com.spring.maven151.model.UserModel;
import com.spring.maven151.service.KaryawanService;
import com.spring.maven151.service.RoleService;
import com.spring.maven151.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private KaryawanService karyawanService;
	
	@RequestMapping(value = "user")
	public String userMethod(Model model) {
		String halaman = "user";
		return halaman;
	}
	
	@RequestMapping(value ="user/list")
	public String listUserMethod(Model model) {
		String halaman = "user/list";
		List<UserModel> userModelList = new ArrayList<UserModel>();
		try {
			userModelList = this.userService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("userModelList", userModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="user/tambah")
	public String tambahUserMethod(Model model) {
		String halaman = "user/add";
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
				
		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("karyawanModelList", karyawanModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value ="user/tambah_simpan")
	public String tambahSimpanUserMethod(HttpServletRequest request) {
		String halaman = "user";
		
		String kodeUser = request.getParameter("kodeUser");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		// Save Join Table
		String kodeRole = request.getParameter("kodeRole");
		String nik = request.getParameter("nik");
		// Save Join Table
		UserModel userModel = new UserModel();
		
		userModel.setKodeUser(kodeUser);
		userModel.setUsername(username);
		userModel.setPassword(password);
		// Save Join Table
		userModel.setKodeRole(kodeRole);
		userModel.setNik(nik);
		// Save Join Table
		
		userModel.setEnabled(1);
		
		try {
			this.userService.create(userModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "user/ubah")
	public String ubahUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/edit";
		String kodeUser = request.getParameter("kodeUser");
		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("userModel",userModel);
		
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
				
		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("karyawanModelList", karyawanModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "user/ubah_simpan")
	public String ubahSimpanUserMethod(HttpServletRequest request) {
		String halaman = "user";
		String kodeUser = request.getParameter("kodeUser");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		// Save Join Table
		String kodeRole = request.getParameter("kodeDepartemen");
		String nik = request.getParameter("nik");
		// Save Join Table
		
		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
			userModel.setUsername(username);
			userModel.setPassword(password);
			// Save Join Table
			userModel.setKodeRole(kodeRole);
			userModel.setNik(nik);
			// Save Join Table
			
			this.userService.update(userModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "user/rincian")
	public String rincianUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/detail";
		String kodeUser = request.getParameter("kodeUser");
		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("userModel",userModel);
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
				
		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("karyawanModelList", karyawanModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "user/hapus")
	public String hapusUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user/delete";
		String kodeUser = request.getParameter("kodeUser");
		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("userModel",userModel);
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
				
		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("karyawanModelList", karyawanModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "user/hapus_simpan")
	public String hapusSimpanUserMethod(HttpServletRequest request, Model model) {
		String halaman = "user";
		String kodeUser = request.getParameter("kodeUser");
		UserModel userModel = new UserModel();
		try {
			userModel = this.userService.cariKode(kodeUser);
			this.userService.delete(userModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("userModel",userModel);
		return halaman;
	}
}
