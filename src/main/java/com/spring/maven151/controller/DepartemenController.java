package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.DepartemenModel;
import com.spring.maven151.service.DepartemenService;

@Controller
public class DepartemenController {
	
	@Autowired
	private DepartemenService departemenService;
	
	@RequestMapping(value = "departemen")
	public String departemenMethod() {
		String halaman = "departemen";
		return halaman;
	}
	
	@RequestMapping(value ="departemen/list")
	public String listDepartemenMethod(Model model) {
		String halaman = "departemen/list";
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();
		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("departemenModelList", departemenModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="departemen/tambah")
	public String tambahDepartemenMethod() {
		String halaman = "departemen/add";
		return halaman;
	}
	
	@RequestMapping(value ="departemen/tambah_simpan")
	public String tambahSimpanDepartemenMethod(HttpServletRequest request) {
		String halaman = "departemen";
		
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String namaDepartemen = request.getParameter("namaDepartemen");
		String telpDepartemen = request.getParameter("telpDepartemen");
		
		DepartemenModel departemenModel = new DepartemenModel();
		
		departemenModel.setKodeDepartemen(kodeDepartemen);
		departemenModel.setNamaDepartemen(namaDepartemen);
		departemenModel.setTelpDepartemen(telpDepartemen);
		
		try {
			this.departemenService.create(departemenModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "departemen/ubah")
	public String ubahDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/edit";
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("departemenModel",departemenModel);
		return halaman;
	}
	
	@RequestMapping(value = "departemen/ubah_simpan")
	public String ubahSimpanDepartemenMethod(HttpServletRequest request) {
		String halaman = "departemen";
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String namaDepartemen = request.getParameter("namaDepartemen");
		String telpDepartemen = request.getParameter("telpDepartemen");
		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
			departemenModel.setNamaDepartemen(namaDepartemen);
			departemenModel.setTelpDepartemen(telpDepartemen);
			this.departemenService.update(departemenModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "departemen/rincian")
	public String rincianDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/detail";
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("departemenModel",departemenModel);
		return halaman;
	}
	
	@RequestMapping(value = "departemen/hapus")
	public String hapusDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen/delete";
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("departemenModel",departemenModel);
		return halaman;
	}
	
	@RequestMapping(value = "departemen/hapus_simpan")
	public String hapusSimpanDepartemenMethod(HttpServletRequest request, Model model) {
		String halaman = "departemen";
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		DepartemenModel departemenModel = new DepartemenModel();
		try {
			departemenModel = this.departemenService.cariKode(kodeDepartemen);
			this.departemenService.delete(departemenModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("departemenModel",departemenModel);
		return halaman;
	}
}
