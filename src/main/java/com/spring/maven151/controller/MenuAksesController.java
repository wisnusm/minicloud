package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.MenuAksesModel;
import com.spring.maven151.model.MenuModel;
import com.spring.maven151.model.RoleModel;
import com.spring.maven151.service.MenuAksesService;
import com.spring.maven151.service.MenuService;
import com.spring.maven151.service.RoleService;

@Controller
public class MenuAksesController extends BaseController{
	
	@Autowired
	private MenuAksesService menuAksesService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "menuAkses")
	public String menuAksesMethod(Model model) {
		String halaman = "menuAkses";
		this.accessLogin(model);
		return halaman;
	}
	
	@RequestMapping(value ="menuAkses/list")
	public String listMenuAksesMethod(Model model) {
		String halaman = "menuAkses/list";
		List<MenuAksesModel> menuAksesModelList = new ArrayList<MenuAksesModel>();
		try {
			menuAksesModelList = this.menuAksesService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("menuAksesModelList", menuAksesModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="menuAkses/tambah")
	public String tambahMenuAksesMethod(Model model) {
		String halaman = "menuAkses/add";
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
				
		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("menuModelList", menuModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value ="menuAkses/tambah_simpan")
	public String tambahSimpanMenuAksesMethod(HttpServletRequest request) {
		String halaman = "menuAkses";
		
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		String kodeRole = request.getParameter("kodeRole");
		String kodeMenu = request.getParameter("kodeMenu");
		
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		
		menuAksesModel.setKodeMenuAkses(kodeMenuAkses);
		menuAksesModel.setKodeRole(kodeRole);
		menuAksesModel.setKodeMenu(kodeMenu);
		
		try {
			this.menuAksesService.create(menuAksesModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "menuAkses/ubah")
	public String ubahMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/edit";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("menuAksesModel",menuAksesModel);
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
				
		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("menuModelList", menuModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "menuAkses/ubah_simpan")
	public String ubahSimpanMenuAksesMethod(HttpServletRequest request) {
		String halaman = "menuAkses";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		String kodeRole = request.getParameter("kodeRole");
		String kodeMenu = request.getParameter("kodeMenu");
		
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);

			menuAksesModel.setKodeMenuAkses(kodeMenuAkses);
			menuAksesModel.setKodeRole(kodeRole);
			menuAksesModel.setKodeMenu(kodeMenu);
			
			this.menuAksesService.update(menuAksesModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "menuAkses/rincian")
	public String rincianMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/detail";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("menuAksesModel",menuAksesModel);
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
				
		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("menuModelList", menuModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "menuAkses/hapus")
	public String hapusMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses/delete";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("menuAksesModel",menuAksesModel);
		//script untuk select combo box dinamis based on data roleMaster
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		
		try {
			roleModelList = this.roleService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("roleModelList", roleModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		//script untuk select combo box dinamis based on data roleMaster
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
				
		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("menuModelList", menuModelList);
		//Akhir script untuk select combo box dinamis based on data roleMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "menuAkses/hapus_simpan")
	public String hapusSimpanMenuAksesMethod(HttpServletRequest request, Model model) {
		String halaman = "menuAkses";
		String kodeMenuAkses = request.getParameter("kodeMenuAkses");
		MenuAksesModel menuAksesModel = new MenuAksesModel();
		try {
			menuAksesModel = this.menuAksesService.cariKode(kodeMenuAkses);
			this.menuAksesService.delete(menuAksesModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("menuAksesModel",menuAksesModel);
		return halaman;
	}
}
