package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.DepartemenModel;
import com.spring.maven151.model.KaryawanModel;
import com.spring.maven151.model.PerusahaanModel;
import com.spring.maven151.service.DepartemenService;
import com.spring.maven151.service.KaryawanService;
import com.spring.maven151.service.PerusahaanService;

@Controller
public class KaryawanController {
	
	@Autowired
	private KaryawanService karyawanService;
	
	@Autowired
	private DepartemenService departemenService;
	
	@Autowired
	private PerusahaanService perusahaanService;
	
	@RequestMapping(value = "karyawan")
	public String karyawanMethod() {
		String halaman = "karyawan";
		return halaman;
	}
	
	@RequestMapping(value ="karyawan/list")
	public String listKaryawanMethod(Model model) {
		String halaman = "karyawan/list";
		List<KaryawanModel> karyawanModelList = new ArrayList<KaryawanModel>();
		try {
			karyawanModelList = this.karyawanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("karyawanModelList", karyawanModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="karyawan/tambah")
	public String tambahKaryawanMethod(Model model) {
		String halaman = "karyawan/add";
		//script untuk select combo box dinamis based on data departemenMaster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();
		
		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("departemenModelList", departemenModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		//script untuk select combo box dinamis based on data departemenMaster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();
				
		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("perusahaanModelList", perusahaanModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		return halaman;
	}
	
	@RequestMapping(value ="karyawan/tambah_simpan")
	public String tambahSimpanKaryawanMethod(HttpServletRequest request) {
		String halaman = "karyawan";
		
		String nik = request.getParameter("nik");
		String namaKaryawan = request.getParameter("namaKaryawan");
		
		//save join table
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		//save join table
		
		KaryawanModel karyawanModel = new KaryawanModel();
		
		karyawanModel.setNik(nik);
		karyawanModel.setNamaKaryawan(namaKaryawan);
		
		//save join table
		karyawanModel.setKodeDepartemen(kodeDepartemen);
		karyawanModel.setKodePerusahaan(kodePerusahaan);
		//save join table
		
		
		try {
			this.karyawanService.create(karyawanModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "karyawan/ubah")
	public String ubahKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/edit";
		String nik = request.getParameter("nik");
		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(nik);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("karyawanModel",karyawanModel);
		//script untuk select combo box dinamis based on data departemenMaster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();
		
		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("departemenModelList", departemenModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		//script untuk select combo box dinamis based on data departemenMaster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();
				
		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("perusahaanModelList", perusahaanModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "karyawan/ubah_simpan")
	public String ubahSimpanKaryawanMethod(HttpServletRequest request) {
		String halaman = "karyawan";
		String nik = request.getParameter("nik");
		String namaKaryawan = request.getParameter("namaKaryawan");
		
		//save join table
		String kodeDepartemen = request.getParameter("kodeDepartemen");
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		//save join table
		
		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(nik);
			karyawanModel.setNamaKaryawan(namaKaryawan);
			//save join table
			karyawanModel.setKodeDepartemen(kodeDepartemen);
			karyawanModel.setKodePerusahaan(kodePerusahaan);
			//save join table
			this.karyawanService.update(karyawanModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "karyawan/rincian")
	public String rincianKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/detail";
		String nik = request.getParameter("nik");
		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(nik);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("karyawanModel",karyawanModel);
		//script untuk select combo box dinamis based on data departemenMaster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();
		
		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("departemenModelList", departemenModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		//script untuk select combo box dinamis based on data departemenMaster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();
				
		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("perusahaanModelList", perusahaanModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "karyawan/hapus")
	public String hapusKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan/delete";
		String nik = request.getParameter("nik");
		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(nik);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("karyawanModel",karyawanModel);
		//script untuk select combo box dinamis based on data departemenMaster
		List<DepartemenModel> departemenModelList = new ArrayList<DepartemenModel>();
		
		try {
			departemenModelList = this.departemenService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		model.addAttribute("departemenModelList", departemenModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		//script untuk select combo box dinamis based on data departemenMaster
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();
				
		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
				
		model.addAttribute("perusahaanModelList", perusahaanModelList);
		//Akhir script untuk select combo box dinamis based on data departemenMaster
		
		return halaman;
	}
	
	@RequestMapping(value = "karyawan/hapus_simpan")
	public String hapusSimpanKaryawanMethod(HttpServletRequest request, Model model) {
		String halaman = "karyawan";
		String nik = request.getParameter("nik");
		KaryawanModel karyawanModel = new KaryawanModel();
		try {
			karyawanModel = this.karyawanService.cariKode(nik);
			this.karyawanService.delete(karyawanModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("karyawanModel",karyawanModel);
		return halaman;
	}
}
