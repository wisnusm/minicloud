package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.SupplierModel;
import com.spring.maven151.service.SupplierService;

@Controller
public class SupplierController extends BaseController{
	
	@Autowired 
	private SupplierService supplierService;
	
	@RequestMapping(value="supplier")
	public String supplierMethod(){
		String halaman = "supplier";
		return halaman;
	}
	
	@RequestMapping(value="supplier/cari")
	public String cariSupplierMethod(Model model, HttpServletRequest request){
		String halaman = "supplier/list";
		
		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");
		
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		try {
			supplierModelList = this.supplierService.cariSupplier(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("supplierModelList", supplierModelList);	
		return halaman;
	}
	
	@RequestMapping(value="supplier/list")
	public String listSupplierMethod(Model model){
		String halaman = "supplier/list";
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();
		
		try {
			/*supplierModelList = this.supplierService.list();*/
			supplierModelList = this.supplierService.listIsNotDelete();
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		model.addAttribute("supplierModelList", supplierModelList);
		return halaman;
	}	
	
	@RequestMapping(value="supplier/simpan_delete")
	public String simpanDeleteSupplierMethod(HttpServletRequest request){
		String halaman = "supplier";
		String kodeSupplier = request.getParameter("kodeSupplier");
		SupplierModel supplierModelDB = new SupplierModel();
		
		try {
			supplierModelDB = this.supplierService.cariKode(kodeSupplier);
			
			//set isdelete audit trail
			String xUpdatedBySupplier = this.getUserModel().getKodeUser();
			supplierModelDB.setxIsDeleteSupplier(1);
			supplierModelDB.setxUpdatedBySupplier(xUpdatedBySupplier);
			supplierModelDB.setxUpdatedDateSupplier(new Date());
			
			this.supplierService.update(supplierModelDB); //karena tidak dihapus secara real
			//this.supplierService.delete(supplierModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/simpan_edit")
	public String simpanEditSupplierMethod(HttpServletRequest request){
		String halaman = "supplier";
		
		String kodeSupplier = request.getParameter("kodeSupplier");
		String namaSupplier = request.getParameter("namaSupplier");

		String tipeSupplier = request.getParameter("tipeSupplier");
		String picSupplier = request.getParameter("picSupplier");
		String teleponSupplier = request.getParameter("teleponSupplier");
		String emailSupplier = request.getParameter("emailSupplier");
		String alamatSupplier = request.getParameter("alamatSupplier");
		
		SupplierModel supplierModelDB = new SupplierModel();
		
		try {
			supplierModelDB = this.supplierService.cariKode(kodeSupplier);
			supplierModelDB.setNamaSupplier(namaSupplier);

			supplierModelDB.setTipeSupplier(tipeSupplier);
			supplierModelDB.setPicSupplier(picSupplier);
			supplierModelDB.setTeleponSupplier(teleponSupplier);
			supplierModelDB.setEmailSupplier(emailSupplier);
			supplierModelDB.setAlamatSupplier(alamatSupplier);
			
			//set AuditTrail
			String xUpdatedBySupplier = this.getUserModel().getKodeUser();
			supplierModelDB.setxUpdatedBySupplier(xUpdatedBySupplier);
			supplierModelDB.setxUpdatedDateSupplier(new Date());
			
			this.supplierService.update(supplierModelDB);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}
		
		return halaman;
	}

	@RequestMapping(value="supplier/simpan_add")
	public String simpanAddSupplierMethod(HttpServletRequest request, Model model) throws Exception{
		String halaman = "supplier";
		
		// cari kode
		String kodeSupplier = request.getParameter("kodeSupplier");
		SupplierModel supplierModelDB = null;
		supplierModelDB = this.supplierService.cariKode(kodeSupplier);
		String kodeSupplierExist = null;
		if (supplierModelDB!=null) {
			kodeSupplierExist = "yes";
		}
		
		// cari nama
		String namaSupplier = request.getParameter("namaSupplier");
		List<SupplierModel> supplierModelListDB = new ArrayList<SupplierModel>();
		supplierModelListDB = this.supplierService.cariNamaSupplier(namaSupplier);
		String namaSupplierExist = null;
		if (supplierModelListDB.size()>0) {
			namaSupplierExist = "yes";
		}
		
		if (kodeSupplierExist=="yes") {
			// g null berarti sudah ada data yg menggunakan kodeSupplier tsb
			model.addAttribute("kodeSupplier", kodeSupplier);
			model.addAttribute("kodeSupplierExist", kodeSupplierExist);
			
		}else if (namaSupplierExist =="yes") {
			// g null berarti sudah ada data yg menggunakan namaSupplier tsb
			model.addAttribute("namaSupplier", namaSupplier);
			model.addAttribute("namaSupplierExist", namaSupplierExist);
			
		} else {
			// null berarti di DB tidak ada data yg menggunakan kodeSupplier tsb
			
			String tipeSupplier = request.getParameter("tipeSupplier");
			String picSupplier = request.getParameter("picSupplier");
			String teleponSupplier = request.getParameter("teleponSupplier");
			String emailSupplier = request.getParameter("emailSupplier");
			String alamatSupplier = request.getParameter("alamatSupplier");
			
			SupplierModel supplierModel = new SupplierModel(); 
			
			supplierModel.setKodeSupplier(kodeSupplier);
			supplierModel.setNamaSupplier(namaSupplier);
			supplierModel.setTipeSupplier(tipeSupplier);
			supplierModel.setPicSupplier(picSupplier);
			supplierModel.setTeleponSupplier(teleponSupplier);
			supplierModel.setEmailSupplier(emailSupplier);
			supplierModel.setAlamatSupplier(alamatSupplier);
			
			//set audit trail
			String xCreatedBySupplier = this.getUserModel().getKodeUser(); //primary key yg diset
			supplierModel.setxCreatedBySupplier(xCreatedBySupplier);
			supplierModel.setxCreatedDateSupplier(new Date());
			supplierModel.setxIsDeleteSupplier(0);
			
			
			this.supplierService.create(supplierModel);
		}
		
		
		
		
		return halaman;
	}
	
	@RequestMapping(value="supplier/detail")
	public String detailSupplierMethod(Model model, HttpServletRequest request){
		String halaman = "supplier/detail";
		String kodeSupplier = request.getParameter("kodeSupplier");
		SupplierModel supplierModel = new SupplierModel();
		try {
			supplierModel = this.supplierService.cariKode(kodeSupplier);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("supplierModel",supplierModel);
		return halaman;
	}
	
	@RequestMapping(value="supplier/delete")
	public String deleteSupplierMethod(Model model, HttpServletRequest request){
		String halaman = "supplier/delete";
		String kodeSupplier = request.getParameter("kodeSupplier");
		SupplierModel supplierModel = new SupplierModel();
		try {
			supplierModel = this.supplierService.cariKode(kodeSupplier);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("supplierModel",supplierModel);
		return halaman;
	}
	
	@RequestMapping(value="supplier/edit")
	public String editSupplierMethod(Model model, HttpServletRequest request){
		String halaman = "supplier/edit";
		String kodeSupplier = request.getParameter("kodeSupplier");
		SupplierModel supplierModel = new SupplierModel();
		try {
			supplierModel = this.supplierService.cariKode(kodeSupplier);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("supplierModel",supplierModel);
		return halaman;
	}
	
	@RequestMapping(value="supplier/tambah")
	public String tambahSupplierMethod(Model model) throws Exception{
		String halaman = "supplier/add";
		Integer seqIdSupplier = this.supplierService.sequenceValueSupplier();
		String kodeSupplierGenerator = null;
		if (seqIdSupplier<10) {
			kodeSupplierGenerator = "SUP000"+seqIdSupplier;
		}else if (seqIdSupplier>=10 && seqIdSupplier<100) {
			kodeSupplierGenerator = "SUP00"+seqIdSupplier;
		}else if (seqIdSupplier>=100 && seqIdSupplier<1000) {
			kodeSupplierGenerator = "SUP0"+seqIdSupplier;
		}else if (seqIdSupplier>=1000) {
			kodeSupplierGenerator = "SUP"+seqIdSupplier;
		}else {
			
		}
		 model.addAttribute("kodeSupplierGenerator", kodeSupplierGenerator);
		
		return halaman;
	}


}
