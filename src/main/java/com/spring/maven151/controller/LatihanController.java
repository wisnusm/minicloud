package com.spring.maven151.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LatihanController {
	
	//Method panggil jsp
	@RequestMapping(value="halaman_daftar")
	public String daftarMethod() {
		String halaman = "daftar";
		return halaman;
	}
	
	@RequestMapping(value="halaman_hasil_daftar")
	public String hasilDaftarMethod(HttpServletRequest request, Model model) {
		String halaman = "daftar/hasil_daftar";
		
		//get value dari name yaitu variable html di jspnya
		//HttpServletRequest request adalah function utk get value dari jsp
		String inputStringA = request.getParameter("inputString");
		Integer inputIntegerA = Integer.valueOf(request.getParameter("inputInteger"));
		
		//set value to variable $ di jsp hasil
		model.addAttribute("outputString", inputStringA);
		model.addAttribute("outputInteger", inputIntegerA);
		
		return halaman;
	}
	


}
