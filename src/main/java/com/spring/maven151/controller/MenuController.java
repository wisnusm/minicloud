package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.MenuModel;
import com.spring.maven151.service.MenuService;

@Controller
public class MenuController extends BaseController{
	
	@Autowired
	private MenuService menuService;
	
	public void accessLogin(Model model) {
		
		//method untuk menampilkan menu by role login dan username
	
		model.addAttribute("username", this.getUserModel().getUsername());
		model.addAttribute("namaRole", this.getUserModel().getRoleModel().getNamaRole());
		
		//logic list menu berdasarkan role
		List<MenuModel> menuModelList = null;
		try {
			menuModelList = this.menuService.getAllMenuTreeByRole(this.getUserModel().getRoleModel().getKodeRole());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		model.addAttribute("menuModelList", menuModelList);
		//akhir method
		
		
	}
	
	@RequestMapping(value = "menu")
	public String menuMethod(Model model) {
		String halaman = "menu";
		
		this.accessLogin(model);
		return halaman;
	}
	
	@RequestMapping(value ="menu/list")
	public String listMenuMethod(Model model) {
		String halaman = "menu/list";
		List<MenuModel> menuModelList = new ArrayList<MenuModel>();
		try {
			menuModelList = this.menuService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("menuModelList", menuModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="menu/tambah")
	public String tambahMenuMethod() {
		String halaman = "menu/add";
		return halaman;
	}
	
	@RequestMapping(value ="menu/tambah_simpan")
	public String tambahSimpanMenuMethod(HttpServletRequest request) {
		String halaman = "menu";
		
		String kodeMenu = request.getParameter("kodeMenu");
		String namaMenu = request.getParameter("namaMenu");
		String controller = request.getParameter("controller");
		
		MenuModel menuModel = new MenuModel();
		
		menuModel.setKodeMenu(kodeMenu);
		menuModel.setNamaMenu(namaMenu);
		menuModel.setController(controller);
		
		try {
			this.menuService.create(menuModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "menu/ubah")
	public String ubahMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/edit";
		String kodeMenu = request.getParameter("kodeMenu");
		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("menuModel",menuModel);
		return halaman;
	}
	
	@RequestMapping(value = "menu/ubah_simpan")
	public String ubahSimpanMenuMethod(HttpServletRequest request) {
		String halaman = "menu";
		String kodeMenu = request.getParameter("kodeMenu");
		String namaMenu = request.getParameter("namaMenu");
		String controller = request.getParameter("controller");
		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
			menuModel.setNamaMenu(namaMenu);
			menuModel.setController(controller);
			this.menuService.update(menuModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "menu/rincian")
	public String rincianMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/detail";
		String kodeMenu = request.getParameter("kodeMenu");
		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("menuModel",menuModel);
		return halaman;
	}
	
	@RequestMapping(value = "menu/hapus")
	public String hapusMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu/delete";
		String kodeMenu = request.getParameter("kodeMenu");
		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("menuModel",menuModel);
		return halaman;
	}
	
	@RequestMapping(value = "menu/hapus_simpan")
	public String hapusSimpanMenuMethod(HttpServletRequest request, Model model) {
		String halaman = "menu";
		String kodeMenu = request.getParameter("kodeMenu");
		MenuModel menuModel = new MenuModel();
		try {
			menuModel = this.menuService.cariKode(kodeMenu);
			this.menuService.delete(menuModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("menuModel",menuModel);
		return halaman;
	}
}
