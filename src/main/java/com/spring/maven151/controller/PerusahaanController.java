package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.PerusahaanModel;
import com.spring.maven151.service.PerusahaanService;

@Controller
public class PerusahaanController {
	
	@Autowired
	private PerusahaanService perusahaanService;
	
	@RequestMapping(value = "perusahaan")
	public String perusahaanMethod() {
		String halaman = "perusahaan";
		return halaman;
	}
	
	@RequestMapping(value ="perusahaan/list")
	public String listPerusahaanMethod(Model model) {
		String halaman = "perusahaan/list";
		List<PerusahaanModel> perusahaanModelList = new ArrayList<PerusahaanModel>();
		try {
			perusahaanModelList = this.perusahaanService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("perusahaanModelList", perusahaanModelList);
		return halaman;		
	}
		
	@RequestMapping(value ="perusahaan/tambah")
	public String tambahPerusahaanMethod() {
		String halaman = "perusahaan/add";
		return halaman;
	}
	
	@RequestMapping(value ="perusahaan/tambah_simpan")
	public String tambahSimpanPerusahaanMethod(HttpServletRequest request) {
		String halaman = "perusahaan";
		
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		String namaPerusahaan = request.getParameter("namaPerusahaan");
		String telpPerusahaan = request.getParameter("telpPerusahaan");
		
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		
		perusahaanModel.setKodePerusahaan(kodePerusahaan);
		perusahaanModel.setNamaPerusahaan(namaPerusahaan);
		perusahaanModel.setTelpPerusahaan(telpPerusahaan);
		
		try {
			this.perusahaanService.create(perusahaanModel);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return halaman;
	}
	
	@RequestMapping(value = "perusahaan/ubah")
	public String ubahPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/edit";
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("perusahaanModel",perusahaanModel);
		return halaman;
	}
	
	@RequestMapping(value = "perusahaan/ubah_simpan")
	public String ubahSimpanPerusahaanMethod(HttpServletRequest request) {
		String halaman = "perusahaan";
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		String namaPerusahaan = request.getParameter("namaPerusahaan");
		String telpPerusahaan = request.getParameter("telpPerusahaan");
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
			perusahaanModel.setNamaPerusahaan(namaPerusahaan);
			perusahaanModel.setTelpPerusahaan(telpPerusahaan);
			this.perusahaanService.update(perusahaanModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return halaman;
	}
	
	@RequestMapping(value = "perusahaan/rincian")
	public String rincianPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/detail";
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("perusahaanModel",perusahaanModel);
		return halaman;
	}
	
	@RequestMapping(value = "perusahaan/hapus")
	public String hapusPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan/delete";
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("perusahaanModel",perusahaanModel);
		return halaman;
	}
	
	@RequestMapping(value = "perusahaan/hapus_simpan")
	public String hapusSimpanPerusahaanMethod(HttpServletRequest request, Model model) {
		String halaman = "perusahaan";
		String kodePerusahaan = request.getParameter("kodePerusahaan");
		PerusahaanModel perusahaanModel = new PerusahaanModel();
		try {
			perusahaanModel = this.perusahaanService.cariKode(kodePerusahaan);
			this.perusahaanService.delete(perusahaanModel);
		} catch (Exception e) {
			e.printStackTrace();
			halaman="error";
		}
		model.addAttribute("perusahaanModel",perusahaanModel);
		return halaman;
	}
}
