package com.spring.maven151.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.maven151.model.ItemModel;
import com.spring.maven151.model.KategoriModel;
import com.spring.maven151.model.SupplierModel;
import com.spring.maven151.service.ItemService;
import com.spring.maven151.service.KategoriService;
import com.spring.maven151.service.SupplierService;

@Controller
public class ItemController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private KategoriService kategoriService;
	
	@Autowired
	private SupplierService supplierService;

	@RequestMapping(value = "item")
	public String ItemMethod() {
		String halaman = "item";
		return halaman;
	}

	@RequestMapping(value = "item/cari")
	public String cariItemMethod(Model model, HttpServletRequest request) {
		String halaman = "item/list";

		String keywordCari = request.getParameter("keywordCari");
		String tipeCari = request.getParameter("tipeCari");

		List<ItemModel> itemModelList = new ArrayList<ItemModel>();
		try {
			itemModelList = this.itemService.cariItem(keywordCari, tipeCari);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("itemModelList", itemModelList);
		return halaman;
	}

	@RequestMapping(value = "item/list")
	public String listItemMethod(Model model) {
		String halaman = "item/list";
		List<ItemModel> itemModelList = new ArrayList<ItemModel>();

		try {
			itemModelList = this.itemService.list();
		} catch (Exception e) {
			// TODO: handle exception

		}
		model.addAttribute("itemModelList", itemModelList);
		return halaman;
	}

	@RequestMapping(value = "item/simpan_delete")
	public String simpanDeleteItemMethod(HttpServletRequest request) {
		String halaman = "item";
		String kodeItem = request.getParameter("kodeItem");
		ItemModel itemModelDB = new ItemModel();

		try {
			itemModelDB = this.itemService.cariKode(kodeItem);
			this.itemService.delete(itemModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "item/simpan_edit")
	public String simpanEditItemMethod(HttpServletRequest request) {
		String halaman = "item";

		String kodeItem = request.getParameter("kodeItem");
		String namaItem = request.getParameter("namaItem");

		String kodeKategori = request.getParameter("kodeKategori");
		String kodeSupplier = request.getParameter("kodeSupplier");
		Integer hargaJualItem = Integer.valueOf(request.getParameter("hargaJualItem"));
		Integer jumlahItem = Integer.valueOf(request.getParameter("jumlahItem"));
		String keteranganItem = request.getParameter("keteranganItem");

		ItemModel itemModelDB = new ItemModel();

		try {
			itemModelDB = this.itemService.cariKode(kodeItem);
			itemModelDB.setNamaItem(namaItem);

			itemModelDB.setKodeKategori(kodeKategori);
			itemModelDB.setKodeSupplier(kodeSupplier);
			itemModelDB.setHargaJualItem(hargaJualItem);
			itemModelDB.setJumlahItem(jumlahItem);
			itemModelDB.setKeteranganItem(keteranganItem);

			this.itemService.update(itemModelDB);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "item/simpan_add")
	public String simpanAddItemMethod(HttpServletRequest request) {
		String halaman = "item";

		String kodeItem = request.getParameter("kodeItem");
		String namaItem = request.getParameter("namaItem");
		
		//save join table
		String kodeKategori = request.getParameter("kodeKategori");
		String kodeSupplier = request.getParameter("kodeSupplier");
		//save join table
		
		Integer hargaJualItem = Integer.valueOf(request.getParameter("hargaJualItem"));
		Integer jumlahItem = Integer.valueOf(request.getParameter("jumlahItem"));
		String keteranganItem = request.getParameter("keteranganItem");

		ItemModel itemModel = new ItemModel();

		itemModel.setKodeItem(kodeItem);
		itemModel.setNamaItem(namaItem);
		
		//save join table
		itemModel.setKodeKategori(kodeKategori);
		itemModel.setKodeSupplier(kodeSupplier);
		//save join table
		
		itemModel.setHargaJualItem(hargaJualItem);
		itemModel.setJumlahItem(jumlahItem);
		itemModel.setKeteranganItem(keteranganItem);

		try {
			this.itemService.create(itemModel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			halaman = "error";
		}

		return halaman;
	}

	@RequestMapping(value = "item/detail")
	public String detailItemMethod(Model model, HttpServletRequest request) {
		String halaman = "item/detail";
		String kodeItem = request.getParameter("kodeItem");
		ItemModel itemModel = new ItemModel();
		try {
			itemModel = this.itemService.cariKode(kodeItem);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("ItemModel", itemModel);
		return halaman;
	}

	@RequestMapping(value = "item/delete")
	public String deleteItemMethod(Model model, HttpServletRequest request) {
		String halaman = "item/delete";
		String kodeItem = request.getParameter("kodeItem");
		ItemModel itemModel = new ItemModel();
		try {
			itemModel = this.itemService.cariKode(kodeItem);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("itemModel", itemModel);
		return halaman;
	}

	@RequestMapping(value = "item/edit")
	public String editItemMethod(Model model, HttpServletRequest request) {
		String halaman = "item/edit";
		String kodeItem = request.getParameter("kodeItem");
		ItemModel itemModel = new ItemModel();
		try {
			itemModel = this.itemService.cariKode(kodeItem);
		} catch (Exception e) {
			// TODO: handle exception
		}
		model.addAttribute("ItemModel", itemModel);
		return halaman;
	}

	@RequestMapping(value = "item/tambah")
	public String tambahItemMethod(Model model) {
		String halaman = "item/add";

		// Skrip utk select combobox dinamis based on data kategoriMaster
		List<KategoriModel> kategoriModelList = new ArrayList<KategoriModel>();

		try {
			kategoriModelList = this.kategoriService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}

		model.addAttribute("kategoriModelList", kategoriModelList);
		// Skrip utk select combobox dinamis based on data kategoriMaster

		// Skrip utk select combobox dinamis based on data supplierMaster
		List<SupplierModel> supplierModelList = new ArrayList<SupplierModel>();

		try {
			supplierModelList = this.supplierService.list();
		} catch (Exception e) {
			// TODO: handle exception
		}

		model.addAttribute("supplierModelList", supplierModelList);
		// Skrip utk select combobox dinamis based on data supplierMaster

		return halaman;
	}

}
