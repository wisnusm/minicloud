package com.spring.maven151.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "M_ITEM")
public class ItemModel {

	private String kodeItem;
	private String namaItem;
	
	private Integer hargaJualItem;
	private Integer jumlahItem;
	private String keteranganItem;
	
	//Join Table ke Kategori
	private String kodeKategori;
	private KategoriModel kategoriModel;
	
	//Join Table ke Supplier
	private String kodeSupplier;
	private SupplierModel supplierModel;
	
	@Id
	@Column(name="KODE_ITEM")
	public String getKodeItem() {
		return kodeItem;
	}
	public void setKodeItem(String kodeItem) {
		this.kodeItem = kodeItem;
	}
	
	@Column(name="NAMA_ITEM")
	public String getNamaItem() {
		return namaItem;
	}
	public void setNamaItem(String namaItem) {
		this.namaItem = namaItem;
	}
	
	@Column(name="HARGA_JUAL_ITEM")
	public Integer getHargaJualItem() {
		return hargaJualItem;
	}
	public void setHargaJualItem(Integer hargaJualItem) {
		this.hargaJualItem = hargaJualItem;
	}
	
	@Column(name="JUMLAH_ITEM")
	public Integer getJumlahItem() {
		return jumlahItem;
	}
	public void setJumlahItem(Integer jumlahItem) {
		this.jumlahItem = jumlahItem;
	}
	
	@Column(name="KETERANGAN_ITEM")
	public String getKeteranganItem() {
		return keteranganItem;
	}
	public void setKeteranganItem(String keteranganItem) {
		this.keteranganItem = keteranganItem;
	}
	
	@Column(name="KODE_KATEGORI")
	public String getKodeKategori() {
		return kodeKategori;
	}
	public void setKodeKategori(String kodeKategori) {
		this.kodeKategori = kodeKategori;
	}
	
	@ManyToOne
	@JoinColumn(name="KODE_KATEGORI", nullable=true,updatable=false, insertable=false)
	public KategoriModel getKategoriModel() {
		return kategoriModel;
	}
	public void setKategoriModel(KategoriModel kategoriModel) {
		this.kategoriModel = kategoriModel;
	}
	
	@Column(name="KODE_SUPPLIER")
	public String getKodeSupplier() {
		return kodeSupplier;
	}
	public void setKodeSupplier(String kodeSupplier) {
		this.kodeSupplier = kodeSupplier;
	}
	
	@ManyToOne
	@JoinColumn(name="KODE_SUPPLIER", nullable=true,updatable=false, insertable=false)
	public SupplierModel getSupplierModel() {
		return supplierModel;
	}
	public void setSupplierModel(SupplierModel supplierModel) {
		this.supplierModel = supplierModel;
	}
	
	

}
