package com.spring.maven151.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "M_DEPARTEMEN")
public class DepartemenModel {

	private String kodeDepartemen;
	private String namaDepartemen;
	private String telpDepartemen;
	
	@Id
	@Column(name="KODE_DEPARTEMEN")
	public String getKodeDepartemen() {
		return kodeDepartemen;
	}
	public void setKodeDepartemen(String kodeDepartemen) {
		this.kodeDepartemen = kodeDepartemen;
	}
	
	@Column(name="NAMA_DEPARTEMEN")
	public String getNamaDepartemen() {
		return namaDepartemen;
	}
	public void setNamaDepartemen(String namaDepartemen) {
		this.namaDepartemen = namaDepartemen;
	}
	
	@Column(name="TELP_DEPARTEMEN")
	public String getTelpDepartemen() {
		return telpDepartemen;
	}
	public void setTelpDepartemen(String telpDepartemen) {
		this.telpDepartemen = telpDepartemen;
	}
	
	
	
	
}
