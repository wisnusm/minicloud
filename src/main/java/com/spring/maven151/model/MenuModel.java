package com.spring.maven151.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "M_MENU")
public class MenuModel {

	private String kodeMenu;
	private String namaMenu;
	private String controller;
	
	@Id
	@Column(name="KODE_MENU")
	public String getKodeMenu() {
		return kodeMenu;
	}
	public void setKodeMenu(String kodeMenu) {
		this.kodeMenu = kodeMenu;
	}
	
	@Column(name="NAMA_MENU")
	public String getNamaMenu() {
		return namaMenu;
	}
	public void setNamaMenu(String namaMenu) {
		this.namaMenu = namaMenu;
	}
	
	@Column(name="CONTROLLER")
	public String getController() {
		return controller;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	
	
	
	
}
