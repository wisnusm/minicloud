package com.spring.maven151.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="M_KATEGORI")
public class KategoriModel {
	
	private String kodeKategori;
	private String namaKategori;
	private String keteranganKategori;
	
	@Id
	@Column(name="KODE_KATEGORI")
	public String getKodeKategori() {
		return kodeKategori;
	}
	public void setKodeKategori(String kodeKategori) {
		this.kodeKategori = kodeKategori;
	}
	
	@Column(name="NAMA_KATEGORI", nullable=true)
	public String getNamaKategori() {
		return namaKategori;
	}
	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
	@Column(name="KETERANGAN_KATEGORI")
	public String getKeteranganKategori() {
		return keteranganKategori;
	}
	public void setKeteranganKategori(String keteranganKategori) {
		this.keteranganKategori = keteranganKategori;
	}
	
	
	

}
