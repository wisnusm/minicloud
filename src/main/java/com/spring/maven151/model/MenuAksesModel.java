package com.spring.maven151.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "M_MENU_AKSES")
public class MenuAksesModel {

	private String kodeMenuAkses;
	
	// join table ke role
	private String kodeRole;
	private RoleModel roleModel;
	
	// join table ke menu
	private String kodeMenu;
	private MenuModel menuModel;
	
	@Id
	@Column(name="KODE_MENU_AKSES")
	public String getKodeMenuAkses() {
		return kodeMenuAkses;
	}
	public void setKodeMenuAkses(String kodeMenuAkses) {
		this.kodeMenuAkses = kodeMenuAkses;
	}
	
	@Column(name="KODE_ROLE")
	public String getKodeRole() {
		return kodeRole;
	}
	public void setKodeRole(String kodeRole) {
		this.kodeRole = kodeRole;
	}
	@ManyToOne
	@JoinColumn(name="KODE_ROLE", nullable=true, updatable=false, insertable=false)
	public RoleModel getRoleModel() {
		return roleModel;
	}
	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}
	
	@Column(name="KODE_MENU")
	public String getKodeMenu() {
		return kodeMenu;
	}
	public void setKodeMenu(String kodeMenu) {
		this.kodeMenu = kodeMenu;
	}
	@ManyToOne
	@JoinColumn(name="KODE_MENU", nullable=true, updatable=false, insertable=false)
	public MenuModel getMenuModel() {
		return menuModel;
	}
	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}
	
	
	
		
}
